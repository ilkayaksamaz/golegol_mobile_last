import React, {Component} from 'react'
import {
    AppRegistry,
    View,
    BackHandler,
    Platform,
    Alert,
    AsyncStorage
} from 'react-native'
import {Provider, connect} from 'react-redux'
import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'

import {RootNavigator, Drawer} from './public/routes'

import indexReducers from './public/reducers/index'
import {addNavigationHelpers, NavigationActions} from 'react-navigation'
import {combineReducers} from 'redux'
import logger from 'redux-logger'
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import {persistStore, autoRehydrate, rehydrationComplete} from 'redux-persist'

XMLHttpRequest = GLOBAL.originalXMLHttpRequest
    ? GLOBAL.originalXMLHttpRequest
    : GLOBAL.XMLHttpRequest;

// fetch logger
global._fetch = fetch;
global.fetch = function (uri, options, ...args) {
    return global
        ._fetch(uri, options, ...args)
        .then((response) => {
            console.log('Fetch', {
                request: {
                    uri,
                    options,
                    ...args
                },
                response
            });
            return response;
        });
};

// this shall be called regardless of app state: running, background or not
// running. Won't be called wh"en app is killed by user in iOS
FCM.on(FCMEvent.Notification, async(notif) => {
    // there are two parts of notif. notif.notification contains the notification
    // payload, notif.data contains data payload
    if (notif.local_notification) {
        //this is a local notification
    }
    if (notif.opened_from_tray) {
        // iOS: app is open/resumed because user clicked banner Android: app is
        // open/resumed because user clicked banner or tapped app icon
    }
    // await someAsyncCall();

    if (Platform.OS === 'ios') {
        // optional iOS requires developers to call completionHandler to end
        // notification process. If you do not call it your background remote
        // notifications could be throttled, to read more about it see the above
        // documentation link. This library handles it for you automatically with
        // default behavior (for remote notification, finish with NoData; for
        // WillPresent, finish depend on "show_in_foreground"). However if you want to
        // return different result, follow the following code to override
        // notif._notificationType is available for iOS platfrom
        switch (notif._notificationType) {
            case NotificationType.Remote:
                notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
                break;
            case NotificationType.NotificationResponse:
                notif.finish();
                break;
            case NotificationType.WillPresent:
                notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
                break;
        }
    }
});
FCM.on(FCMEvent.RefreshToken, (token) => {
    console.log(token)
    // fcm token may not be available on first load, catch it here
});

const store = createStore(indexReducers, undefined, compose(applyMiddleware(...[thunk, logger]), autoRehydrate()))

class RootContainer extends Component {
    
    handleBackPress = () => {
        // const {dispatch, nav} = this.props; const activeRoute =
        // nav.routes[nav.index]; if (activeRoute.index === 0) { return false; }
        // dispatch(NavigationActions.back()); return true;

    };
    componentDidMount() {

        persistStore(store, {
            storage: AsyncStorage,
            whitelist: ["userInfo","token"]
        }, () => {
            this.setState({rehydrated: true})
        });

        // iOS: show permission prompt for the first call. later just check permission
        // in user settings Android: check permission in user settings
        FCM
            .requestPermissions()
            .then(() => console.log('granted'))
            .catch(() => console.log('notification permission rejected'));

        FCM
            .getFCMToken()
            .then(token => {
                console.log(token)
                // store fcm token in your server
            });

        FCM.subscribeToTopic('/topics/all');

        this.notificationListener = FCM.on(FCMEvent.Notification, async(notif) => {
            switch (notif.Type) {}
            console.log(notif);
        });

        // initial notification contains the notification that launchs the app. If user
        // launchs app by clicking banner, the banner notification info will be here
        // rather than through FCM.on event sometimes Android kills activity when app
        // goes to background, and when resume it broadcasts notification before JS is
        // run. You can use FCM.getInitialNotification() to capture those missed events.
        // initial notification will be triggered all the time even when open app by
        // icon so send some action identifier when you send notification
        FCM
            .getInitialNotification()
            .then(notif => {
                console.log(notif)
            });

        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }
    componentWillUnmount() {
        this
            .notificationListener
            .remove();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
    render() {
        const {dispatch, nav} = this.props;
        return (
            <Drawer/>
        );
    }
}

export default class Golegol extends Component {
    constructor(props) {
        super(props)
        console.log(props)
    }
    render() {
        return (
            <Provider store={store}>
                <RootContainer/>
            </Provider>
        )
    }
}

AppRegistry.registerComponent('golegol', () => Golegol);
