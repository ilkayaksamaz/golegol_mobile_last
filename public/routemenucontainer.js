import {connect} from 'react-redux'
import DrawerMenu from './drawermenu'
import {logout} from './actions/signin'

const mapStateToProps = (state) => ({nav: state.nav, userInfo: state.userInfo});

const mapDispatchToProps = (dispatch) => ({
    logout: (userInfo, isAuth) => {
        dispatch(logout())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerMenu)
