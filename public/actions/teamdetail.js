export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN'})
    }
}

export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE'})
    }
}