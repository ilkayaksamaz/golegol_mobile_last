export function disableSounds(isDisabled = false) {
    return (dispatch, getState) => {
        dispatch({ type: 'DISABLE_SOUND', isDisabled })
    }
}

export function disableNotifications(isDisabled = false) {
    return (dispatch, getState) => {
        dispatch({ type: 'DISABLE_NOTIF', isDisabled })
    }
}