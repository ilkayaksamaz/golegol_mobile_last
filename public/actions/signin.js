export function login(user) {
    return (dispatch, getState) => {
        dispatch({type: 'LOGIN', user})
    }
}

export function logout() {
    return (dispatch, getState) => {
        dispatch({type: 'LOGOUT'})
    }
}

export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN'})
    }
}



export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE'})
    }
}
export function setToken(token) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_TOKEN', token})
    }
}

export function isAuthenticated(user) {
    return (dispatch, getState) => {
        dispatch({type: 'ISAUTHENTICATED', user})
    }
}