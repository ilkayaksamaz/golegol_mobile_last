export function setSelectedLeague(league) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_LEAGUE', league})
    }
}
export function setAllLeagues(leagues) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_ALL_LEAGUES', leagues})
    }
}

export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN'})
    }
}



export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE'})
    }
}