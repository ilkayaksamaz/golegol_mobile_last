export function setSelectedSeason(season) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_LEAGUE_SEASON', season})
    }
}

export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN'})
    }
}



export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE'})
    }
}