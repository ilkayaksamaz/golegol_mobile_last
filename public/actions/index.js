export function getMatches(match) {
    return (dispatch, getState) => {
        dispatch({type: 'GET_MATCHES', match})
    }
}

export function setLive(isLive) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_LIVE', isLive})
    }
}

export function clearMatches() {
    return (dispatch, getState) => {
        dispatch({type: 'CLEAR_MATCHES'})
    }
}

export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN', spinner: true})
    }
}

export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE', spinner: false})
    }
}

export function setCurrentPart(part) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_PART', part})
    }
}

export function getCurrentPart() {
    return (dispatch, getState) => {
        dispatch({
            type: 'GET_CURRENT_PART',
            part: getState().parts
        })
    }
}

export function setCurrentLiveScoreDate(date) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_LIVESCORE_DATE', date})
    }
}

export function getCurrentLiveScoreDate() {
    return (dispatch, getState) => {
        dispatch({
            type: 'GET_CURRENT_LIVESCORE_DATE',
            livescoreDate: getState().livescoreDate
        })
    }
}

export function setCurrentOrderType(order) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_ORDER_TYPE', order})
    }
}

export function setSelectedLeague(league) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_LEAGUE', league})
    }
}

export function setAllLeagues(leagues) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_ALL_LEAGUES', leagues})
    }
}
export function starEnabled(isEnabled) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_STAR_ENABLED', isEnabled})
    }
}