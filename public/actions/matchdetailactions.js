export function setSelectedMatch(matchdetail) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_MATCH', matchdetail})
    }
}

export function setSelectedMatchInfo(info) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_MATCH_INFO', info})
    }
}

export function setSelectedMatchHomeSquad(info) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_MATCH_HOME_SQUAD', info})
    }
}


export function setSelectedMatchAwaySquad(info) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_MATCH_AWAY_SQUAD', info})
    }
}

export function setSelectedMatchLeague(league) {
    return (dispatch, getState) => {
        dispatch({type: 'SET_CURRENT_MATCH_LEAGUE', league})
    }
}

export function spinnerOpen() {
    return (dispatch, getState) => {
        dispatch({type: 'OPEN'})
    }
}



export function spinnerClose() {
    return (dispatch, getState) => {
        dispatch({type: 'CLOSE'})
    }
}