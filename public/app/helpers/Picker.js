import React, {Component} from 'react'
import {View, Picker} from 'react-native'

export class PickerWrapper extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let width = this.props.width;
        let height = this.props.height;
        let selected = this.props.selected;
        let onUpdate = this.props.onUpdate;
        let color = this.props.color;
        let dataSouce = this
            .props
            .dataSource
            .map((m, i) => <Picker.Item label={m.label} value={m.value}  key={i}/>);

        return (
            <View
                style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                alignSelf: 'stretch',
                width: width,
                height: height
            }}>
                <Picker
                    selectedValue={selected}
                    onValueChange={onUpdate}
                    prompt='Tüm Ligler'
                    style={[{
                        color: color,
                        width: width,
                        height: height,
                        alignItems: 'flex-end'
                    }
                ]}>

                    {dataSouce}
                </Picker>

            </View>

        )
    }
}