import React from 'react'
import {DatePickerAndroid, DatePickerIOS, Platform, Alert} from 'react-native'

export class DatePicker {
    static async open(selectedOptions) {
        
        if (Platform.OS == "ios") {
            var options = Object.assign({
                date: new Date(),
                mode:'date'
            }, selectedOptions);
            try {
                return await DatePickerIOS.open(options);
            } catch ({code, message}) {
                console.warn('Cannot open date picker', message);
            }
        }
        if (Platform.OS == "android") {
           
            var options = Object.assign({
                date: new Date(),
            }, selectedOptions);
            try {
                return await DatePickerAndroid.open(options);
            } catch ({code, message}) {
                console.warn('Cannot open date picker', message);
            }
        }
    }
}