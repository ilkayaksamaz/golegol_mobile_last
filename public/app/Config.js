const api = 'https://golegol.com';

export const APP_CONFIG = {
    'livescoreservice': api + "/api/",
    'domain': 'https://golegol.com',
    'tokenEndPoint': api + '/token'
};
