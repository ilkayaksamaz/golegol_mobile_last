import React from 'react'
import {APP_CONFIG} from './Config'
export class ConnectionManager {
    constructor() {}

    static request(url, params = {}, fromCache = false, cacheKey = "") {
        var options = Object.assign({
            data: '',
            type: 'POST',
            cache: false,
            completed: function (resultJSON) {}
        }, params);

        var headers = {
            "Content-Type": "application/json"
        };

        fetch(APP_CONFIG.livescoreservice + url, options).then(response => {
            if (response.status >= 200 && response.status < 300) {

                // var separators = ['hepsi=', '&futbol=', '&basketbol=']; var splitted =
                // data.replace(new RegExp("@", 'g'), "'").split(new
                // RegExp(separators.join('|'), 'g'));
                //
                return response;
            } else {
                let error = new Error(response.statusText);
                    error.response = response;
                    throw error;
                }
            })
            .then(response => response.json())
            .then(data => {
                options.completed(data);
            })
            .catch((error) => {
                console.error(error);
            });
    }
    static requestSync(url, params = {}, fromCache = false, cacheKey = "") {
        var options = Object.assign({
            data: '',
            type: 'POST',
            cache: false,
            completed: function (resultJSON) {}
        }, params);

        var headers = {
            "Content-Type": "application/json"
        };

        return fetch(APP_CONFIG.livescoreservice + url, options);
    }
}