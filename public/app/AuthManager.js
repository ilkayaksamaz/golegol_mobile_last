import React from 'react'
import {AsyncStorage} from 'react-native'

import {APP_CONFIG} from './Config'
import {paths} from './helpers/apipaths'
import FCM from 'react-native-fcm';

const tokenKey = '@userTore:token';
const usernameKey = '@userTore:username';
const passwordKey = '@userTore:password';

const userInfoInitialState = {
    Password: "",
    RePassword: "",
    UserName: "",
    Email: "",
    Phone: ""
}

export class AuthManager {
    constructor() {}

    static clearAuthToken(resolve) {
        AsyncStorage
            .removeItem(tokenKey)
            .then(resolve)
    }
    static async getUserInfo() {
        return AsyncStorage
            .getAllKeys()
            .then((keys) => {
                const fetchKeys = keys.filter((k) => {
                    return k.startsWith('@userTore:');
                });
                return AsyncStorage.multiGet(fetchKeys);
            })
            .then((result) => {
                return result.reduce(function (acc, i, store) {
                    let key = i[0].replace('@userTore:', '');
                    let val = i[1];
                    acc[key] = val;
                    return acc;
                }, {});
                return obj
            });
    }
    static login(username, password, resolve) {
        var params = {
            username: username,
            password: password,
            grant_type: "password"
        };

        var formBody = [];
        for (var property in params) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(params[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        var options = {
            body: formBody,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            method: 'POST',
            cache: false
        };

        return fetch(APP_CONFIG.tokenEndPoint, options).then(response => {
            if ((response.status == 500)) {
                let error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
            return response;
        })
            .then(response => response.json())
            .then(data => {
                var st = {
                    status: "ok",
                    token: "",
                    username: ""
                }
                if (data.access_token && data.access_token != "") {
                    AsyncStorage.setItem(usernameKey, username);
                    AsyncStorage.setItem(passwordKey, password);
                    AsyncStorage.setItem(tokenKey, data.access_token);
                    st.data = data.access_token
                    st.username = data.userName
                } else {
                    st.status = "failed"
                }
                return st;
            })
            .then(resolve)
            .catch((error) => {
                console.error(error);
            });
    }

    static create(userInfo = userInfoInitialState) {
        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userInfo),
            method: 'POST'
        };

        return fetch(APP_CONFIG.livescoreservice + paths.useradd, options)
            .then(response => {
            return response;
        })
            .then(response => response.json())
            .catch((error) => {
                console.error(error);
            });
    }

    static addComment(token, message, entityId, entityName) {
        var params = {
            Message: message,
            EntityId: entityId,
            EntityName: entityName
        };
        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify(params),
            method: 'POST'
        };

        return fetch(APP_CONFIG.livescoreservice + paths.forumadd, options)
            .then(response => {
            return response;
        })
            .then(response => response.json())
            .catch((error) => {
                console.error(error);
            });
    }

    static addBookmark(token, entityId, entityName, displayName, sportType, isSelected) {
        FCM.subscribeToTopic('/topics/' + entityName + '_' + entityId);
        var params = {
            EntityId: entityId,
            EntityName: entityName,
            SportType: sportType,
            IsSelected: isSelected,
            DisplayName: displayName
        };

        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify(params),
            method: 'POST'
        };

        return fetch(APP_CONFIG.livescoreservice + paths.addbookmark, options)
            .then(response => {
            return response;
        })
            .then(response => response.json())
            .catch((error) => {
                console.error(error);
            });
    }

    static getbookmark(token) {
        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            method: 'GET'
        };

        return fetch(APP_CONFIG.livescoreservice + paths.getbookmark, options)
            .then(response => {
            return response;
        })
            .then(response => response.json())
            .catch((error) => {
                console.error(error);
            });
    }

    static deleteBookmark(token, entityId, entityName) {
        var params = {
            entityId,
            entityName
        };
        var options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            body: JSON.stringify(params),
            method: 'POST'
        };

        return fetch(APP_CONFIG.livescoreservice + paths.deletebookmark, options)
            .then(response => {
            return response;
        })
            .then(response => response.json())
            .catch((error) => {
                console.error(error);
            });
    }
}