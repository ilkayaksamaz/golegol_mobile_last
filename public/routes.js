import React, { Component } from 'react'
import {
    StatusBar,
    Text,
    Button,
    TouchableOpacity,
    Image,
    ScrollView,
    View,
    StyleSheet,
    Platform
} from 'react-native';

import {
    StackNavigator,
    TabNavigator,
    DrawerNavigator,
    DrawerView,
    DrawerItems,
    TabBarTop
} from 'react-navigation';

import DrawerMenuContainer from './routemenucontainer';

import IndexComponent from './containers/IndexContainer';
import LeaguesListView from './containers/LeaguesContainer';
import LeagueScoreContainer from './containers/LeagueScoreContainer';
import MatchDetailContainerView from './containers/MatchDetailContainer';
import SeasonPickerContainer from './containers/SeasonPickerContainer';
import LeagueScoreStaticsContainer from './containers/LeagueScoreStaticsContainer';
import LeagueStaticsFixtureContainer from './containers/LeagueStaticsFixtureContainer';
import TeamDetailContainer from './containers/TeamDetailContainer';
import SignInContainer from './containers/SignInContainer';
import SignUpContainer from './containers/SignUpContainer';

import MatchInfo from './components/HelperComponentes/MatchInfo';
import Favourites from './components/Favourites';
import Settings from './containers/SettingsContainer';
import MatchCompare from './components/HelperComponentes/MatchCompare';
import MatchForum from './components/HelperComponentes/MatchForum';
import SearchContainer from './containers/SearchContainer';
import MatchLeague from './components/HelperComponentes/MatchLeague';
import TeamDetailMatches from './components/TeamDetailMatches';
import TeamDetailSquad from './components/TeamDetailSquad';
import FootballerDetail from './components/FootballerDetail';
import FootballerPersonelInfo from './components/FootballerPersonelInfo';
import FootballerDetailCarear from './components/FootballerDetailCarear';

import DetailHeader from './components/HelperComponentes/DetailHeader';
import { AuthManager } from './app/AuthManager'

class HeaderStar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            params: {
                entityName: "",
                id: 0,
                selected: false,
                type: 0
            }
        }
    }
    componentDidMount() {
        if (this.props.state != undefined) {
            let params = this.props.state.navigation.state.params;
            this.setState({ selected: this.props.state.navigation.state.params.selected, params: params });
        }
    }
    render() {
        let props = this.props;
        let params = this.state.params;
        let image = this.state.selected
            ? require("./content/images/header-star-selected.png")
            : require("./content/images/header-star.png");
        return (
            <TouchableOpacity
                onPress={() => AuthManager.getUserInfo().then((user) => {
                    AuthManager
                        .addBookmark(user.token, params.id, params.entityName, params.name, params.type, params.selected)
                        .then((res) => {
                            this.setState({ selected: res });
                        });
                })}>
                <Image
                    style={{
                        marginRight: 20
                    }}
                    source={image}></Image>
            </TouchableOpacity>
        )
    }
}
let naviOptions = {
    headerRight: <HeaderStar />,
    leftButtonTitle: '',
    headerTitleStyle: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: '600',
        textAlign: 'center'
    },
    headerStyle: {
        backgroundColor: '#e4b713'
    },
    headerTintColor: 'white'
}
const HomeStack = StackNavigator({
    Home: {
        screen: IndexComponent,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: null,
            headerRight: null,
            header: null
        })
    },
    Favourites: {
        screen: Favourites,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: "Favorilerim",
            headerRight: null
        })
    },
    Settings: {
        screen: Settings,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: "Ayarlar",
            headerRight: null
        })
    },
    LeaguesList: {
        screen: LeaguesListView,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Tüm Ligler',
            headerRight: null
        })
    },
    LeagueScore: {
        screen: LeagueScoreContainer,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Puan Durumu',
            headerRight: null
        })
    },
    SeasonPicker: {
        screen: SeasonPickerContainer,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Sezon',
            headerRight: null
        })
    },
    MatchDetail: {
        screen: MatchDetailContainerView,
        title: null,
        navigationOptions: (all) => Object.assign({}, naviOptions, {
            headerTitle: 'Maç',
            headerRight: <HeaderStar state={all} />
        })
    },
    FootballerDetail: {
        screen: FootballerDetail,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Oyuncu',
            headerRight: null
        })
    },
    TeamDetail: {
        screen: TeamDetailContainer,
        title: null,
        navigationOptions: (all) => Object.assign({}, naviOptions, {
            headerTitle: 'Takım',
            headerRight: <HeaderStar state={all} />
        })
    },
    SignIn: {
        screen: SignInContainer,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Giriş Yap',
            headerRight: null
        })
    },
    SignUp: {
        screen: SignUpContainer,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Üye Ol',
            headerRight: null
        })
    },
    Search: {
        screen: SearchContainer,
        title: null,
        navigationOptions: Object.assign({}, naviOptions, {
            headerTitle: 'Arama',
            headerRight: null
        })
    }

}, { headerMode: 'screen' });

export const FootbalDetailTabs = TabNavigator({
    MatchInfo: {
        screen: MatchInfo,
        navigationOptions: Object.assign({}, naviOptions, { title: 'Maç' })
    },
    MatchCompare: {
        screen: MatchCompare,
        navigationOptions: Object.assign({}, naviOptions, { title: 'Kıyas' })
    },
    MatchLeague: {
        screen: MatchLeague,
        navigationOptions: Object.assign({}, naviOptions, { title: 'Lig' })
    },
    MatchForum: {
        screen: MatchForum,
        navigationOptions: Object.assign({}, naviOptions, { title: 'Forum' })
    }
}, {
        lazy: true,

        tabBarComponent: (props) => (<TabBarTop {...props} />),
        tabBarPosition: 'top',
        tabBarOptions: {
            activeBackgroundColor: '#3f3f3f',
            inactiveBackgroundColor: '#07772c',
            labelStyle: {
                fontSize: 12
            },
            style: {
                backgroundColor: '#0e8d38'
            }
        }
    });

export const LeagueScoreDetailTabs = TabNavigator({
    Score: {
        screen: LeagueScoreStaticsContainer,
        navigationOptions: Object.assign({}, naviOptions, { title: 'LİG DURUMU' })
    },
    Fixture: {
        screen: LeagueStaticsFixtureContainer,
        navigationOptions: Object.assign({}, naviOptions, { title: 'HAFTANIN MAÇLARI' })
    }
}, {
        lazy: true,
        tabBarComponent: (props) => (<TabBarTop {...props} />),
        tabBarPosition: 'top',
        tabBarOptions: {
            activeBackgroundColor: '#3f3f3f',
            inactiveBackgroundColor: '#07772c',
            labelStyle: {
                fontSize: 14
            },
            style: {
                backgroundColor: '#0e8d38'
            }
        }
    });

export const MatchDetailTabs = TabNavigator({
    Mathes: {
        screen: TeamDetailMatches,
        navigationOptions: Object.assign({}, naviOptions, { title: 'MAÇ' })
    },
    Squad: {
        screen: TeamDetailSquad,
        navigationOptions: Object.assign({}, naviOptions, { title: 'KADRO' })
    }
}, {
        lazy: true,
        tabBarComponent: (props) => (<TabBarTop {...props} />),
        tabBarPosition: 'top',
        tabBarOptions: {
            activeBackgroundColor: '#3f3f3f',
            inactiveBackgroundColor: '#07772c',
            labelStyle: {
                fontSize: 14
            },
            style: {
                backgroundColor: '#0e8d38'
            }
        }
    })

export const FootballerDetailTabs = TabNavigator({
    PersonelInfo: {
        screen: FootballerPersonelInfo,
        navigationOptions: Object.assign({}, naviOptions, { title: 'KİŞİSEL BİGLİLER' })
    },
    Carear: {
        screen: FootballerDetailCarear,
        navigationOptions: Object.assign({}, naviOptions, { title: 'KARİYER' })
    }
}, {
        lazy: true,
        tabBarComponent: (props) => (<TabBarTop {...props} />),
        tabBarPosition: 'top',
        tabBarOptions: {
            activeBackgroundColor: '#3f3f3f',
            inactiveBackgroundColor: '#07772c',
            labelStyle: {
                fontSize: 14
            },
            style: {
                backgroundColor: '#0e8d38'
            }
        }
    })
export const RootNavigator = StackNavigator({
    Home: {
        screen: HomeStack
    },
    FootballDetailTabs: {
        screen: FootbalDetailTabs
    },
    LeagueScoreDetailTabs: {
        screen: LeagueScoreDetailTabs
    },
    MatchDetailTabs: {
        screen: MatchDetailTabs
    },
    FootballerDetailTabs: {
        screen: FootballerDetailTabs
    }
}, {
        headerMode: 'none'
    }, );

const CustomDrawerContentComponent = (props) => (
    <View style={{
        flex: 1
    }}>
        <Text>test</Text>
    </View>
);

export const SearchDrawer = DrawerNavigator({
    Search: {
        screen: RootNavigator,
        navigationOptions: Object.assign({}, naviOptions, { title: 'CANLI SONUÇLAR' })
    }
}, {
        contentComponent: (props) => <CustomDrawerContentComponent screenProps={props} />,
        drawerPosition: 'right',
        navigationOptions: {}
    });

export const Drawer = DrawerNavigator({
    Home: {
        screen: RootNavigator,
        navigationOptions: Object.assign({}, naviOptions, { title: 'CANLI SONUÇLAR' })
    }
}, {
        contentComponent: (props) => <DrawerMenuContainer screenProps={props} />
    });
