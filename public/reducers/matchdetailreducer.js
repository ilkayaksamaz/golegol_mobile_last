const initialState = {
    League: {},
    Home: {
        TeamLogo: {},
        Coach: {}
    },
    Away: {
        TeamLogo: {},
        Coach: {}
    },
    Cards: [],
    MatchParts: {
        FH: {
            Home: 0,
            Away: 0
        },
        MS: {
            Home: 0,
            Away: 0
        }
    },
    Time: {},
    Stadium: {},
    MatchEvent: []
}

export function matchdetail(state = initialState, action) {
    switch (action.type) {
        case "SET_CURRENT_MATCH":
            return action.matchdetail;
        default:
            return state
    }
}

export function matchdetailinfo(state = [], action) {
    switch (action.type) {
        case "SET_CURRENT_MATCH_INFO":
            return action.info;
        default:
            return state
    }
}

export function homesquad(state = [], action) {
    switch (action.type) {
        case "SET_CURRENT_MATCH_HOME_SQUAD":
            return action.info;
        default:
            return state
    }
}

export function awaysquad(state = [], action) {
    switch (action.type) {
        case "SET_CURRENT_MATCH_AWAY_SQUAD":
            return action.info;
        default:
            return state
    }
}

export function matchdetailleague(state = [], action) {
    switch (action.type) {
        case "SET_CURRENT_MATCH_LEAGUE":
            return action.league;
        default:
            return state
    }
}