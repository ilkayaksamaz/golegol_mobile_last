export function leagues(state = [], action) {
    switch (action.type) {
        case "SET_ALL_LEAGUES":
            return action.leagues;
        default:
            return state
    }
}

export function selectedLeague(state = -1, action) {
    switch (action.type) {
        case "SET_CURRENT_LEAGUE":
            return action.league;
        default:
            return state
    }
}
