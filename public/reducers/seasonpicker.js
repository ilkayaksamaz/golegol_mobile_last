const currentYear = new Date();
var year = currentYear.getFullYear();
var month = currentYear.getMonth();
var day = currentYear.getDate();

const initialValue = new Date().getFullYear() + "/" + new Date(currentYear.getFullYear() + 1, month, day).getFullYear()

export function season(state = initialValue, action) {
    switch (action.type) {
        case "SET_CURRENT_LEAGUE_SEASON":
            return action.season;
        default:
            return state
    }
}
