import { combineReducers } from 'redux'
import nav from './nav'
import { leagues, selectedLeague } from './leagues'
import { } from './leaguescore'
import { } from './leaguescorestatics'
import { } from './leaguestaticsfixture'
import { userInfo, token } from './signin'
import { } from './signup'
import { season } from './seasonpicker'
import { matchdetail, matchdetailinfo, awaysquad, homesquad, matchdetailleague } from './matchdetailreducer'

import { REHYDRATE } from 'redux-persist'

function matches(state = [], action) {
    switch (action.type) {
        case "GET_MATCHES":
            // for (var i in action.match) {
            //     var m = action.match[i];
            //     state.push(m);
            // }
            return action.match
        case "CLEAR_MATCHES":
            state = [];
            return state
        default:
            return state
    }
}

function parts(state = "Football", action) {
    switch (action.type) {
        case "SET_CURRENT_PART":
            return action.part;
        case "GET_CURRENT_PART":
            return state;
        default:
            return state
    }
}

function dates(state = new Date(), action) {
    switch (action.type) {
        case "SET_CURRENT_LIVESCORE_DATE":
            return action.date;
        case "GET_CURRENT_LIVESCORE_DATE":
            return state;
        default:
            return state
    }
}

function order(state = "byLeague", action) {
    switch (action.type) {
        case "SET_CURRENT_ORDER_TYPE":
            return action.order;
        default:
            return state
    }
}

function isLive(state = false, action) {
    switch (action.type) {
        case "SET_LIVE":
            return action.isLive;
        default:
            return state
    }
}

function isStartEnabled(state = false, action) {
    switch (action.type) {
        case "SET_STAR_ENABLED":
            return action.isEnabled;
        default:
            return state
    }
}

function spinner(state = false, action) {
    switch (action.type) {
        case "OPEN":
            return action.spinner;
        case "CLOSE":
            return action.spinner;
        default:
            return state
    }
}


function soundIsDisabled(state = false, action) {
    switch (action.type) {
        case "DISABLE_SOUND":
            return action.isDisabled;
        case REHYDRATE:
            const savedData = action.isDisabled || state;
            return savedData;
        default:
            return state
    }
}
function notificationIsDisabled(state = false, action) {
    switch (action.type) {
        case "DISABLE_NOTIF":
            return action.isDisabled;
        case REHYDRATE:
            const savedData = action.isDisabled || state;
            return savedData;
        default:
            return state
    }
}
const indexReducers = combineReducers({
    soundIsDisabled,
    notificationIsDisabled,
    matches,
    parts,
    dates,
    order,
    isLive,
    leagues,
    selectedLeague,
    matchdetail,
    nav,
    matchdetailinfo,
    awaysquad,
    homesquad,
    matchdetailleague,
    spinner,
    season,
    userInfo,
    token,
    isStartEnabled
});

export default indexReducers