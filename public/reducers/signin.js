import {AuthManager} from './../app/AuthManager'
import {REHYDRATE} from 'redux-persist'

let userInfoIntialState = {
    userName: "",
    password: "",
    isAuthenticated: false
}

export function userInfo(state = userInfoIntialState, action) {
    switch (action.type) {
        case "LOGIN":
            return action.user;
        case "ISAUTHENTICATED":
            return action.user;
        case "LOGOUT":
            return userInfoIntialState;
        case REHYDRATE:
            const savedData = action.payload.userInfo || userInfoIntialState;
            return savedData;
        default:
            return state;
    }
}

export function token(state = "", action) {
    switch (action.type) {
        case "SET_TOKEN":
            return action.token;

        default:
            return state
    }
}
