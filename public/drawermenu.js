import React, {Component} from 'react'
import {AuthManager} from './app/AuthManager'
import {
    StatusBar,
    Text,
    Button,
    TouchableOpacity,
    Image,
    ScrollView,
    View,
    StyleSheet,
    Platform
} from 'react-native';

export default class Menu extends Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        console.log(this.props.userInfo)
    }
    onItemSelected(page) {
        this
            .props
            .screenProps
            .navigation
            .navigate(page);
    }
    render() {
        let {navigate} = this.props.screenProps.navigation;
        return (
            <ScrollView scrollsToTop={false} style={styles.menu}>
                <View style={styles.avatarContainer}>
                    {(this.props.userInfo.isAuthenticated && <View style={styles.textHolder}>
                        <Text style={styles.name} numberOfLines={3}>{this.props.userInfo.userName}</Text>
                        <TouchableOpacity
                            onPress={() => {
                            this
                                .props
                                .logout();
                            navigate("DrawerClose");
                        }}>
                            <Text style={styles.logoutButton}>Çıkış Yap</Text>
                        </TouchableOpacity>
                    </View>)}
                    {(!this.props.userInfo.isAuthenticated && <View>
                        <View style={styles.textHolder}>
                            <TouchableOpacity onPress={() => navigate("SignIn")}>
                                <Text style={styles.logoutButton}>Giriş Yap</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.textHolder}>
                            <TouchableOpacity onPress={() => navigate("SignUp")}>
                                <Text style={styles.logoutButton}>Üye ol</Text>
                            </TouchableOpacity>
                        </View>
                    </View>)}

                </View>
                <View style={styles.menuItems}>
                    <View style={styles.childMenu}>
                        <Image
                            style={styles.menuItemBack}
                            source={require('./content/images/football-menu-backround.png')}>
                            <Text onPress={() => this.onItemSelected('About')} style={styles.item}>
                                FUTBOL
                            </Text>
                        </Image>
                        <Text onPress={() => this.onItemSelected('Home')} style={styles.subItem}>
                            Canlı Sonuçlar
                        </Text>
                        <Text onPress={() => this.onItemSelected('LeagueScore')} style={styles.subItem}>
                            Puan Durumu
                        </Text>

                    </View>
                    <View style={styles.childMenu}>
                        <Text
                            onPress={() => this.onItemSelected('Favourites')}
                            style={[styles.subItem, styles.settingMargin]}>
                            Favorilerim
                        </Text>
                        <Text onPress={() => this.onItemSelected('Settings')} style={styles.subItem}>
                            Bildirim Ayarlarım
                        </Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#e4b713'
    },
    avatarContainer: {
        flex: .2,
        padding: 20,
        backgroundColor: '#ffffff',
        paddingTop: Platform.OS === "ios"
            ? 35
            : 15
    },
    name: {
        color: '#e4b713',
        fontSize: 18,
        fontWeight: '700'
    },
    textHolder: {
        flex: 1,
        justifyContent: 'center'
    },
    logoutButton: {
        color: '#1b1b1b',
        fontSize: 14,
        fontWeight: '600'
    },
    menuItems: {
        flex: .8,
        backgroundColor: '#e4b713',
        padding: 20
    },
    childMenu: {},
    menuItemBack: {
        marginLeft: -20,
        paddingLeft: 70,
        marginTop: 20,
        justifyContent: 'center'
    },
    item: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '700',
        flexWrap: 'wrap',
        flexDirection: 'column',
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: 'transparent'
    },
    subItem: {

        color: '#ffffff',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 32,
        paddingTop: 5,
        paddingBottom: 5,
        borderBottomColor: '#e9c542',
        borderBottomWidth: 1
    },
    settingMargin: {
        marginTop: 40
    }

});