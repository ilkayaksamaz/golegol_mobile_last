import React, { Component } from 'react'
import { View, Text, Image, ImageBackground, StyleSheet, ScrollView } from 'react-native'

import { paths } from './../app/helpers/apipaths'
import moment from 'moment';
import { ConnectionManager } from './../app/ConnectionManager'
import { APP_CONFIG } from './../app/Config'

class FootballerPersonelInfo extends Component {
    constructor(props) {
        super(props)

    }
    render() {
        let footballerDetail = this.props.screenProps.state.footballerDetail;
        let imageAvaible = footballerDetail.PlayerInfo.ImagePath != null;
        let footballerImage = APP_CONFIG.domain + footballerDetail.PlayerInfo.ImagePath;

        return (
            <ScrollView style={{
                backgroundColor: '#e7e7e7',
                flexDirection: 'column',
                flex: 1
            }}>
                <View style={{
                    justifyContent: 'flex-end',
                    flex: 1,
                }} >
                    <View style={{
                        justifyContent: 'flex-end',
                        paddingTop: 30,
                        paddingLeft: 10,
                        paddingRight: 10
                    }}>

                        <View style={styles.firection}>
                            {imageAvaible && (
                                <Image style={{
                                    marginRight: 10, width: 49, height: 55,
                                    borderColor: '#c4c4c4',
                                    borderWidth: 3,
                                    backgroundColor: '#ffffff',

                                }}
                                    source={{ uri: footballerImage }}
                                />
                            )}
                        </View>
                        <View style={styles.firection}>
                            <Text style={{
                                color: '#3f3f3f',
                                fontSize: 23,
                                fontWeight: '700',
                            }}>{footballerDetail.Name}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center', marginBottom: 40 }}>
                            <Text style={{
                                color: '#0e8d38',
                                fontSize: 11,
                                fontWeight: '600',
                            }}>{footballerDetail.Team.Name} - </Text>
                            <Text style={{
                                color: '#0e8d38',
                                fontSize: 11,
                                fontWeight: '600',
                            }}>{footballerDetail.PlayerInfo.Position}</Text>
                        </View>

                        <View style={styles.firection}>
                            <View style={[
                                styles.property
                            ]}>
                                <View style={[styles.propertyItem, styles.propertyItemLeft]}>
                                    <Text>Doğum Yeri</Text>
                                </View>
                                <View style={[styles.propertyItem, styles.propertyItemRight]}>
                                    <Text>{footballerDetail.PlayerInfo.BirthPlace}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.seperator}></View>

                        <View style={styles.firection}>
                            <View style={[
                                styles.property
                            ]}>
                                <View style={[styles.propertyItem, styles.propertyItemLeft]}>
                                    <Text>Doğum Tarihi</Text>
                                </View>
                                <View style={[styles.propertyItem, styles.propertyItemRight]}>

                                    <Text>{moment(footballerDetail.PlayerInfo.BirthDate).format('DD.MM.YYYY')}</Text>

                                </View>
                            </View>
                        </View>
                        <View style={styles.seperator}></View>

                        <View style={styles.firection}>
                            <View style={[
                                styles.property
                            ]}>
                                <View style={[styles.propertyItem, styles.propertyItemLeft]}>
                                    <Text>Kontrat Sonu</Text>
                                </View>
                                <View style={[styles.propertyItem, styles.propertyItemRight]}>

                                    <Text>{moment(footballerDetail.PlayerInfo.EndOfContract).format('DD.MM.YYYY')}</Text>

                                </View>
                            </View>
                        </View>
                        <View style={styles.seperator}></View>

                        <View style={[styles.firection, { marginBottom: 40 }]} >
                            <View style={[
                                styles.property
                            ]}>
                                <View style={[styles.propertyItem, styles.propertyItemLeft]}>
                                    <Text>Boy Kilo</Text>
                                </View>
                                <View style={[styles.propertyItem, styles.propertyItemRight]}>
                                    <Text>{footballerDetail.PlayerInfo.Height} {footballerDetail.PlayerInfo.Weight}</Text>
                                </View>

                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    firection: {
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    property: {
        flexDirection: 'row',
    },
    propertyItem: {
        flex: .5
    },
    propertyItemLeft: {
        alignItems: 'flex-start'
    },
    propertyItemRight: {
        alignItems: 'flex-end'
    },
    seperator: {
        height: 1,
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 20
    },
})

export default FootballerPersonelInfo
