import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image
} from 'react-native'
import {paths} from './../app/helpers/apipaths'

import {ConnectionManager} from './../app/ConnectionManager'

export default class LeagueScoreView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scoreTable: [],
            isApplyed: true,
            dataStatus: 1,
            selectedLeague: -1,
            season: ""
        }

    }

    componentWillUnmount() {
        let localProps = this.props.screenProps;
        localProps.setSelectedLeague(-1);
    }
    componentDidMount() {
        let localProps = this.props.screenProps;
        let isParamsDefined = localProps.navigation.state.params != undefined;
        var leagueId = isParamsDefined
            ? localProps.navigation.state.params.leagueId != undefined
                ? localProps.navigation.state.params.leagueId
                : 1
            : 1;

        ConnectionManager.request(paths.leagueList, {
            completed: function (e) {
                localProps.setAllLeagues(e);
                if (localProps.selectedLeague == -1) {
                    localProps.setSelectedLeague(1);
                } else {
                    localProps.setSelectedLeague(localProps.selectedLeague);
                }
            }
        }, true, "allLeagues");
    }
    fetchResponses(league, season) {
        let uri = paths.leagueStatics + "/" + league + "?seasonValue=" + season + "&options=" + this.state.dataStatus;
        ConnectionManager.request(uri, {
            completed: function (e) {
                this.setState({scoreTable: e, isApplyed: false})
            }.bind(this)
        });
    }

    _keyExtractor = (item, index) => item.takim_id;
    componentWillReceiveProps(nextProps) {
        this.setState({selectedLeague: nextProps.selectedLeague, season: nextProps.season})
        this.fetchResponses(nextProps.selectedLeague, nextProps.season);
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    isActive(val) {
        return this.state.dataStatus == val;
    }
    onPress(val) {
        this.setState({
            dataStatus: val
        }, () => {
            this.fetchResponses(this.state.selectedLeague, this.state.season);
        })
    }

    nagigateToTeam(teamId) {
        const {navigate} = this.props.screenProps.navigation;
        navigate("TeamDetail", {id: teamId})
    }
    render() {
        let props = this.props;
        const {navigate} = props.navigation;
        let league = this
            .props
            .leagues
            .find(function (m) {
                return m.value == props.selectedLeague;
            });
        let currentLeague = league == undefined
            ? "Tüm Ligler"
            : league.label;
        return (
            <View
                style={{
                backgroundColor: '#ffffff',
                flex: 1
            }}>
                <View style={styles.tabsContainer}>
                    <View
                        style={[
                        styles.tabsButton, this.isActive(1)
                            ? styles.tabsButtonActive
                            : {}
                    ]}>
                        <TouchableOpacity onPress={() => this.onPress(1)}>
                            <Text style={styles.tabsButtonText}>Genel</Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={[
                        styles.tabsButton, this.isActive(2)
                            ? styles.tabsButtonActive
                            : {}
                    ]}>
                        <TouchableOpacity onPress={() => this.onPress(2)}>
                            <Text style={styles.tabsButtonText}>İçerde</Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={[
                        styles.tabsButton, this.isActive(3)
                            ? styles.tabsButtonActive
                            : {}, {
                            marginRight: 0
                        }
                    ]}>
                        <TouchableOpacity onPress={() => this.onPress(3)}>
                            <Text style={[styles.tabsButtonText]}>Dışarda</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={{
                    backgroundColor: '#3f3f3f',
                    paddingTop: 8,
                    paddingBottom: 8,
                    paddingLeft: 20,
                    paddingRight: 20
                }}>
                    <Text
                        style={{
                        color: '#ffffff',
                        fontSize: 11,
                        fontWeight: '600'
                    }}>{currentLeague}</Text>
                </View>
                <View style={{
                    flex: 1
                }}>
                    <FlatList
                        data={this.state.scoreTable}
                        keyExtractor={this._keyExtractor}
                        ListHeaderComponent={() => <View
                        style={{
                        flex: 1,
                        flexDirection: 'row',
                        backgroundColor: '#5d5d5d',
                        paddingTop: 8,
                        paddingBottom: 8
                    }}>
                        <View
                            style={[{
                                flex: 0.1,
                                alignItems: 'center'
                            }
                        ]}></View>
                        <View style={styles.big}>
                            <Text style={[styles.text, styles.color]}>Takım</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>O</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>G</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>B</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>M</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>A</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>Y</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>P</Text>
                        </View>
                        <View style={styles.small}>
                            <Text style={[styles.text, styles.color]}>AV</Text>
                        </View>
                    </View>}
                        renderItem={({item, index}) => <View
                        style={{
                        flex: 1,
                        flexDirection: 'row'
                    }}>
                        <View
                            style={[
                            {
                                flex: .1,
                                alignItems: 'center'
                            },
                            styles.margin
                        ]}>
                            <Text style={styles.text}>{index + 1}</Text>
                        </View>
                        <View style={[styles.big, styles.margin]}>
                            <TouchableOpacity onPress={() => this.nagigateToTeam(item.takim_id)}>
                                <Text style={styles.text}>{item.takim_ad}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_o}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_g}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_b}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_m}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_a}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_y}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={styles.text}>{item.oz_puan}</Text>
                        </View>
                        <View style={[styles.small, styles.margin]}>
                            <Text style={[styles.text]}>{item.oz_av}</Text>
                        </View>
                    </View>}/>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    tabsContainer: {
        flexDirection: 'row',
        backgroundColor: '#0e8d38',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10
    },
    tabsButton: {
        flex: 1,
        height: 37,
        justifyContent: 'center',
        marginRight: 5,
        backgroundColor: '#07772c',
        padding: 10
    },
    tabsButtonActive: {
        backgroundColor: '#3f3f3f'
    },
    tabsButtonText: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '400'
    },
    big: {
        flex: 0.3,
        paddingLeft: 20,
        paddingRight: 20
    },
    small: {
        flex: 0.09
    },
    margin: {
        paddingBottom: 4,
        paddingTop: 4,
        borderBottomWidth: 1,
        borderBottomColor: '#e7e7e7'
    },
    text: {
        color: '#1c1c1c',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20
    },
    color: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600'
    }
})