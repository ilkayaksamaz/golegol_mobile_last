import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image,
    KeyboardAvoidingView

} from 'react-native'
import {paths} from './../app/helpers/apipaths'
import moment from 'moment';
import {ConnectionManager} from './../app/ConnectionManager'
import {APP_CONFIG} from './../app/Config'
import {FootbalDetailTabs} from './../routes'

export default class MatchDetailView extends Component {
    constructor(props) {
        super(props);
        this._fetchResponses();
        this.state = {
            render: false
        };
    }

    _fetchResponses() {
        let props = this.props;
        let current = this;
        ConnectionManager.request(paths.matchDetail + "/" + props.navigation.state.params.id, {
            completed: function (e) {
                props.setSelectedMatch(e.Match);
                this.setState({render: true});
            }.bind(this)
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.matchdetail.Home === undefined || this.props.matchdetail.Home.Id == undefined) {
            return false
        }
        return true;
    }

    render() {
        if (this.state.render) {
            let props = this.props;
            let detail = props.matchdetail;
            let noImage = require("../content/images/noimage.png");
            let stadiumImage = require("../content/images/stadium-icon.png");
            let refreeImage = require("../content/images/refree-icon.png");
            let homeImage = detail.Home.TeamLogo.LogoPath != undefined
                ? {
                    uri: APP_CONFIG.domain + detail.Home.TeamLogo.LogoPath
                }
                : noImage;
            let awayImage = detail.Away.TeamLogo.LogoPath != undefined
                ? {
                    uri: APP_CONFIG.domain + detail.Away.TeamLogo.LogoPath
                }
                : noImage;
            return (
                <View
                    style={{
                    flex: 1,
                    backgroundColor: '#ffffff'
                }}>
                    <View style={styles.container}>
                        <View style={styles.headerContainer}>
                            <View style={styles.headerTeamDetailPart}>
                                <View style={styles.headerTeamInfo}>
                                    <View>
                                        <View style={styles.headerTeamInfoImageWrapper}>
                                            <Image source={homeImage} style={styles.headerTeamInfoImage}></Image>
                                        </View>
                                    </View>
                                    <View>
                                        <Text style={styles.teamName} numberOfLines={4}>{detail.Home.Name}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.coachName} numberOfLines={4}>{detail.Home.Coach.Name}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.headerTeamDetailPart}>
                                <View>
                                    <View>
                                        <Text style={styles.headerDateFormat}>{moment(new Date(detail.Time.Date)).format("DD.MM.YYYY")}</Text>
                                    </View>
                                    <View>
                                        <View style={styles.scoreWrapper}>
                                            <Text style={styles.scoreText}>{detail.MatchParts.MS.Home}
                                                - {detail.MatchParts.MS.Away}</Text>
                                        </View>
                                        <View>
                                            <Text style={styles.firstHalf}>İY: {detail.MatchParts.FH.Home}
                                                - {detail.MatchParts.FH.Away}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.headerTeamDetailPart}>
                                <View style={styles.headerTeamInfo}>
                                    <View>
                                        <View style={styles.headerTeamInfoImageWrapper}>
                                            <Image source={awayImage} style={styles.headerTeamInfoImage}></Image>
                                        </View>
                                    </View>
                                    <View>
                                        <Text style={styles.teamName} numberOfLines={4}>{detail.Away.Name}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.coachName} numberOfLines={4}>{detail.Away.Coach.Name}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.headerInfoBar}>
                        <View style={styles.headerInfoBarContentWrapper}>
                            <View style={styles.headerInfoBarContentPart}>
                                <View style={styles.headerInfoBarContent}>
                                    <Image source={stadiumImage} style={styles.headerInfoBarIcon}></Image>
                                </View>
                                <View style={styles.headerInfoBarContent}>
                                    <Text style={styles.headerInfoBarContentText} numberOfLines={4}>{detail.Stadium.Name}</Text>
                                </View>
                            </View>
                            <View style={styles.headerInfoBarContentPart}>
                                <View style={styles.headerInfoBarContent}>
                                    <Image
                                        source={refreeImage}
                                        style={{
                                        width: 19,
                                        height: 10
                                    }}></Image>
                                </View>
                                <View style={styles.headerInfoBarContent}>
                                    <Text style={styles.headerInfoBarContentText} numberOfLines={1}>{detail.Away.Name}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                        flex: 1,
                        justifyContent: 'flex-start'
                    }}>
                        <KeyboardAvoidingView
                            behavior="padding"
                            style={{
                            flex: 1
                        }}>
                            <FootbalDetailTabs screenProps={props}/>
                        </KeyboardAvoidingView>
                    </View>
                </View>
            );
        } else {
            return (null)
        }
    }
}
const styles = StyleSheet.create({
    container: {
        paddingLeft: 8,
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 8,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    headerInfoBar: {},
    headerInfoBarContentWrapper: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#e5e5e5',
        height: 30,
        paddingRight: 30,
        paddingLeft: 30,
        flexDirection: 'row',
        alignSelf: 'stretch'
    },
    headerInfoBarContent: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginRight: 10
    },
    headerInfoBarContentPart: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    headerInfoBarContentText: {
        color: '#3f3f3f',
        fontSize: 11,
        fontWeight: '400',
        lineHeight: 11
    },
    headerInfoBarIcon: {
        width: 21,
        height: 13
    },
    headerContainer: {
        flexDirection: 'row'
    },
    headerTeamDetailPart: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    headerDateFormat: {
        color: '#858585',
        fontSize: 14,
        fontWeight: '400',
        marginTop: 15
    },
    headerTeamInfo: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTeamInfoImage: {
        width: 31,
        height: 35
    },
    headerTeamInfoImageWrapper: {
        borderColor: '#c4c4c4',
        borderStyle: 'solid',
        borderWidth: 3,
        backgroundColor: '#ffffff',
        paddingBottom: 8,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 8,
        height: 58
    },
    textAlignCenter: {
        textAlign: 'center'
    },
    teamName: {
        color: '#3f3f3f',
        lineHeight: 12,
        fontWeight: '600',
        marginTop: 5,
        textAlign: 'center'
    },
    coachName: {
        fontSize: 8,
        fontWeight: '400',
        marginTop: 3
    },
    scoreWrapper: {
        borderColor: '#757575',
        borderStyle: 'solid',
        borderWidth: 4,
        backgroundColor: '#757575',
        borderRadius: 10,
        marginTop: 20
    },
    scoreText: {
        color: '#ffffff',
        fontSize: 29,
        textAlign: 'center',
        fontWeight: '700'
    },
    firstHalf: {
        color: '#1c1c1c',
        fontSize: 11,
        fontWeight: '700',
        textAlign: 'center',
        marginTop: 5
    }
})