import React, {Component} from 'react'
import {View, Text, ScrollView, StyleSheet, TouchableOpacity} from 'react-native'
import Star from './HelperComponentes/BookmarkStar'
import {AuthManager} from './../app/AuthManager'
class FavouriteView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            token: "",
            FavouriteTeams: [],
            FavouriteMatches: []
        }
        console.log(props)
    }

    componentDidMount() {
        AuthManager
            .getUserInfo()
            .then(user => {
                this.setState({token: user.token})
                AuthManager
                    .getbookmark(user.token)
                    .then((res) => {
                        this.setState({
                            FavouriteTeams: res.filter(p => {
                                return p.EntityName == "TeamDetail"
                            }),
                            FavouriteMatches: res.filter(p => {
                                return p.EntityName == "MatchDetail"
                            })
                        })
                        console.log(res);
                    })
            })
    }
    render() {
        let {navigate} = this.props.navigation;

        var teams = this
            .state
            .FavouriteTeams
            .map((d) => <View style={styles.contentContainer}>
                <View
                    style={{
                    flex: 1,
                    flexDirection: 'row'
                }}>
                    <View style={{
                        flex: 1
                    }}>
                        <Text>
                            {d.DisplayName}
                        </Text>
                    </View>
                    <View
                        style={{
                        flex: 1,
                        alignItems: 'flex-end'
                    }}>
                        <Star
                            updateSelected={(isSelected) => {}}
                            selected={true}
                            eventId={d.EntityId}
                            sportType={d.SportType}
                            name={d.DisplayName}
                            screenProps={{
                            token: this.state.token,
                            address: "TeamDetail"
                        }}/>
                    </View>
                </View>
            </View>);

        var matches = this
            .state
            .FavouriteMatches
            .map((d) => <View style={styles.contentContainer}>
                <View
                    style={{
                    flex: 1,
                    flexDirection: 'row'
                }}>
                    <View style={{
                        flex: 1
                    }}>
                        <Text>
                            {d.DisplayName}
                        </Text>
                    </View>
                    <View
                        style={{
                        flex: 1,
                        alignItems: 'flex-end'
                    }}>
                        <Star
                            updateSelected={(isSelected) => {}}
                            selected={true}
                            eventId={d.EntityId}
                            sportType={d.SportType}
                            name={d.DisplayName}
                            screenProps={{
                            token: this.state.token
                        }}/>
                    </View>
                </View>
            </View>);
        return (
            <View
                style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: '#ffffff'
            }}>

                <ScrollView>
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Favori Takımlar</Text>
                    </View>

                    {teams}
                    <View style={styles.header}>
                        <Text style={styles.headerText}>Maçlar</Text>
                    </View>
                    {matches}
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        backgroundColor: '#3f3f3f',
        flex: 1,
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    contentContainer: {
        padding: 10,
        flexDirection: 'row',
        flex: 1
    },
    headerText: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600'
    }
})
export default FavouriteView