import React, {Component} from 'react'
import {
    View,
    Text,
    ScrollView,
    RefreshControl,
    Image,
    StyleSheet,
    SectionList,
    TouchableOpacity,
    Platform,
    Sound
} from 'react-native'

import {ConnectionManager} from '../app/ConnectionManager'
import {AuthManager} from '../app/AuthManager'
import FootballLine from './HelperComponentes/FootballLine'
import Header from './HelperComponentes/IndexHeader'
import {paths} from './../app/helpers/apipaths'
import IndexFilterBar from './HelperComponentes/IndexFilterBar'
import Spinner from 'react-native-loading-spinner-overlay';

let apiPollIntervalId
class IndexComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            page: 1,
            spinnerisactive: false
        };
    }

    componentDidMount() {
        this._fetchResponses = this
            ._fetchResponses
            .bind(this);
        this._onRefresh = this
            ._onRefresh
            .bind(this);

        this._onRefresh();

        apiPollIntervalId = setInterval(this._fetchResponses, 10000)
    }

    componentWillUnmount() {
        clearInterval(apiPollIntervalId)
    }

    _fetchResponses() {
        AuthManager
            .getUserInfo()
            .then((userInfo) => {
                let props = this.props;
                let headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + userInfo.token
                };
                let current = this;
                var moment = require('moment')
                let date = this.props.dates.day
                    ? moment(new Date(this.props.dates.year, this.props.dates.month, this.props.dates.day)).format('DDMMYYYY')
                    : ""
                var url = paths.liveScore + "?returnType=" + props.order + "&page=" + current.state.page + "&type=" + props.parts + "&selectedLeague=" + props.selectedLeague + "&onlyLives=" + this.props.isLive + "&isStartEnabled=" + this.props.isStartEnabled + "&matchDate=" + date;

                ConnectionManager.request(url, {
                    headers: headers,
                    completed: function (e) {
                        props.onGetMatches(e);
                        current.setState({refreshing: false, spinnerisactive: false});
                    }
                });
            })

    }
    componentWillUpdate(nextProps, nextState) {
        if (this.props.selectedLeague !== nextProps.selectedLeague) {
            this._onRefresh();
        }
    }

    // shouldComponentUpdate(nextProps, nextState) {     if
    // (this.props.selectedLeague !== nextProps.selectedLeague) {         return
    // true;     }     if (this.props.order !== nextProps.order) {         return
    // true;     }     if (this.state.page !== nextState.page) {         return
    // true;     }     if (this.props.parts !== nextProps.parts) {         return
    // true;     }     if (this.props.isLive !== nextProps.isLive) {         return
    // true;     }     if (this.props.isStartEnabled !== nextProps.isStartEnabled) {
    //         return true;     }      if (this.props.matches.length !==
    // nextProps.matches.length) {         return true;     }     return false; }

    _onRefresh = () => {
        this.setState({
            refreshing: true,
            spinnerisactive: true,
            page: 1
        }, () => {
            this._fetchResponses()
        });

    }
    handleLoadMore = () => {
        let page = this.state.page;
        this.setState({
            refreshing: true,
            page: page + 1
        }, () => {
            this._fetchResponses()
        });
    }

    render() {
        let props = this.props;
        return (
            <View
                style={{
                flex: 1,
                paddingTop: Platform.OS === "ios"
                    ? 25
                    : 0,
                backgroundColor: '#ffffff'
            }}>
                <Spinner visible={this.state.spinnerisactive} textContent={""}/>
                <Header {...props}/>
                <IndexFilterBar
                    {...props}
                    uptedateFunction={() => {
                    this._onRefresh();
                }}/>
                <SectionList
                    sections={this.props.matches}
                    onRefresh={this._onRefresh}
                    refreshing={this.state.refreshing}
                    renderItem={({item}) => <FootballLine item={item} {...this.props}/>}
                    keyExtractor={(item, index) => item.Id}
                    renderSectionHeader={({section}) => section.title
                    ? <Text style={styles.leagueContainer}>{section.title}</Text>
                    : null}/>
            </View>
        )
    }
}

//
const styles = StyleSheet.create({
    leagueContainer: {
        backgroundColor: '#5d5d5d',
        flex: 0,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 20,
        fontSize: 11,
        fontWeight: '600',
        color: '#ffffff'
    },
    barContainer: {
        backgroundColor: '#3f3f3f',
        flex: 0,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 20
    },
    league: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    leagueText: {
        color: '#ffffff'
    },
    date: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    dateText: {
        color: '#ffffff'
    }
})
export default IndexComponent
