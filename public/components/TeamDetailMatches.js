import React, { Component } from 'react'
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native'

import { APP_CONFIG } from './../app/Config'
import { ConnectionManager } from './../app/ConnectionManager'
import { paths } from './../app/helpers/apipaths'
import moment from 'moment'
class TeamDetailMatches extends Component {
    constructor(props) {
        super(props)
        this.state = {
            matches: []
        }
        console.log(this.props.screenProps)
    }

    componentDidMount() {
        this.fetchReponses();
    }


    fetchReponses() {
        ConnectionManager.request(paths.teamDetailMatches + "/" + this.props.screenProps.state.teamId, {
            completed: function (e) {
                this.setState({ matches: e });
            }.bind(this)
        });
    }
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    marginTop: 5,
                    marginBottom: 5,

                    flex: 1,
                    backgroundColor: "#e7e7e7",
                }}
            />
        );
    };

    nagigateToTeam(teamId) {
        const { navigate } = this.props.screenProps.props.navigation;
        navigate("TeamDetail", { id: teamId })
    }

    nagigateToMatch(id) {
        const { navigate } = this.props.screenProps.props.navigation;
        navigate("MatchDetail", { id: id })
    }
    render() {
        let props = this.props.screenProps;
        let teamDetail = props.state.teamDetail;
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={{ backgroundColor: '#3f3f3f', padding: 10 }}>
                    <Text style={{
                        color: '#ffffff',
                        fontSize: 11,
                        fontWeight: '600',
                    }}>
                        {teamDetail.Country}
                    </Text>
                </View>
                <View style={{ flex: 1, padding: 10 }}>
                    <FlatList
                        style={{ flex: 1 }}
                        automaticallyAdjustContentInsets={false}
                        data={this.state.matches}
                        keyExtractor={(item, index) => "" + item.HomeId + item.AwayId + index}
                        ItemSeparatorComponent={this.renderSeparator}
                        renderItem={({ item }) =>
                            <View style={{
                                flex: 1,
                                flexDirection: 'row'
                            }}>
                                <View style={{
                                    flex: .19,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                }}>
                                    <Text style={[styles.textStyle]}>  {moment(item.EventDate).format('DD.MM.YY')}</Text>
                                </View>
                                <View style={{
                                    flex: .3,
                                    justifyContent: 'flex-end', alignItems: 'center', flexDirection: 'row'
                                }}>
                                    <TouchableOpacity
                                        onPress={() => this.nagigateToTeam(item.HomeId)}>
                                        <Text style={styles.textStyle}>{item.HomeName}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View
                                    style={{ flex: .2, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }} >

                                    <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => this.nagigateToMatch(item.Id)}>
                                        <Text style={styles.textStyle}>{item.HomeScore} - </Text>
                                        <Text style={styles.textStyle}>{item.AwayScore}</Text>
                                    </TouchableOpacity>

                                </View>
                                <View style={{ flex: .3, justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row' }}>
                                    <TouchableOpacity
                                        onPress={() => this.nagigateToTeam(item.AwayId)}>
                                        <Text style={styles.textStyle}>{item.AwayName}</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: '#1c1c1c',
        fontSize: 12,
        lineHeight: 20,
    },
    container: {

    }
})

export default TeamDetailMatches