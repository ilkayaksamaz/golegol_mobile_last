import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet
} from 'react-native'
import { paths } from './../app/helpers/apipaths'
import { APP_CONFIG } from './../app/Config'
import { ConnectionManager } from './../app/ConnectionManager'
import { MatchDetailTabs } from './../routes'

class TeamDetailView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            teamId: props.navigation.state.params.id,
            teamDetail: {
                Id: -1,
                Name: "",
                TeamLogo: {
                    LogoPath: null
                },
                Country: ""
            }
        }
    }

    componentDidMount() {
        this.fetchReponses();
    }
    fetchReponses() {

        ConnectionManager.request(paths.teamDetail + "/" + this.state.teamId, {
            completed: function (e) {
                if (e == null) {
                    this.props.navigation.goBack();
                } else {
                    this.setState({ teamDetail: e });
                }
            }.bind(this)
        });
    }
    render() {
        let teamDetail = this.state.teamDetail;

        let noImage = require("./../content/images/noimage.png");
        let imageAvaible = teamDetail.TeamLogo.LogoPath != null;
        let teamImage = APP_CONFIG.domain + "/assets/images/" + teamDetail.TeamLogo.LogoPath;
        if (teamDetail.Id != -1) {
            return (
                <View style={{
                    flexDirection: 'column',
                    flex: 1,
                    backgroundColor: '#ffffff'
                }}>
                    <View style={{ flexDirection: 'row', padding: 20, }}>
                        <View style={{
                            flex: .2,
                            width: 34,
                            height: 60,
                            justifyContent: 'center',
                            marginRight: 10
                        }}>
                            {imageAvaible && (
                                <Image style={{ width: 34, height: 38 }} height={38} width={34}
                                    source={{ uri: teamImage }}
                                />
                            )}
                        </View>
                        <View style={{
                            flex: 1,
                            height: 60,
                            justifyContent: 'center'
                        }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                <Text style={styles.teamName}>{this.state.teamDetail.Name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ marginRight: 5 }}>
                                    <Image source={require("./../content/images/couch-icon.png")} />
                                </View>
                                <View>
                                    <Text style={styles.coachName}>{this.state.teamDetail.CoachName}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{ flex: 1 }}>
                        <MatchDetailTabs screenProps={{
                            props: this.props,
                            state: this.state
                        }} />
                    </View>
                </View>
            )
        } else {
            return null;
        }
    }
}

const styles = StyleSheet.create({
    teamName: {
        fontSize: 18,
        fontWeight: '600',
        color: '#3f3f3f',
        marginBottom: 5
    },
    coachName: {
        fontSize: 11,
        fontWeight: '400',
        color: '#3f3f3f',

    },
})


export default TeamDetailView