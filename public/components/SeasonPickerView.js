import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet
} from 'react-native'
import {paths} from './../app/helpers/apipaths'

import {ConnectionManager} from './../app/ConnectionManager'

export default class SeasonPickerView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allSeasons: []
        }
    }

    componentDidMount() {
        let localProps = this.props;
        ConnectionManager.request(paths.season, {
            completed: function (e) {
                this.setState({allSeasons: e})
            }.bind(this)
        }, true, "allSeasons");
    }
    render() {
        let {goBack} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <FlatList
                        automaticallyAdjustContentInsets={false}
                        data={this.state.allSeasons}
                        keyExtractor={(item, index) => item.Label}
                        renderItem={({item}) => <TouchableOpacity
                        onPress={() => {
                        this
                            .props
                            .setSelectedSeason(item.Label);
                        goBack();
                    }}>
                        <Text>
                            {item.Label}
                        </Text>
                    </TouchableOpacity>}/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 20,
        backgroundColor: '#ffffff'
    },
    textContainer: {
        flexDirection: 'row',
        flex: 0,
        alignItems: 'stretch'
    },
    input: {
        height: 35,
        alignItems: 'stretch',
        flex: 1,
        marginBottom: 20,

        color: '#828282',
        fontSize: 14,
        fontWeight: '400',
        borderColor: '#c8c8c8',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        opacity: 0.8
    }
})