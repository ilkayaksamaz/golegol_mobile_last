import React, { Component } from 'react'
import {
    View, Text, FlatList, Image, StyleSheet,
    TouchableOpacity
} from 'react-native'
import { paths } from './../app/helpers/apipaths'
import moment from 'moment';
import { ConnectionManager } from './../app/ConnectionManager'
import { APP_CONFIG } from './../app/Config'
class TeamDetailSquadView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            squad: []
        }

    }
    componentDidMount() {
        this.fetchReponses();
    }
    _keyExtractor = (item, index) => item.Id;
    fetchReponses() {
        ConnectionManager.request(paths.squad + "/" + this.props.screenProps.state.teamId, {
            completed: function (h) {
                this.setState({
                    squad: h
                });
            }.bind(this)
        });
    }
    render() {
        let { navigate } = this.props.screenProps.props.navigation;
        return (

            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <FlatList
                    data={this.state.squad}
                    keyExtractor={this._keyExtractor}
                    ListHeaderComponent={() =>
                        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#3f3f3f', padding: 10 }}>
                            <View style={styles.small}>
                                <Text style={styles.textStyle}>No</Text>
                            </View>
                            <View style={styles.big}>
                                <Text style={styles.textStyle}>Oyuncu</Text>
                            </View>
                            <View style={styles.medium}>
                                <Text style={styles.textStyle}>P </Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.textStyle}> Yaş</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.textStyle}>M</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.textStyle}>11</Text>
                            </View>
                            <View style={styles.small}>
                                <Image source={require('./../content/images/goal.png')} />
                            </View>
                            <View style={styles.small}>
                                <Image source={require('./../content/images/red-card.png')} />
                            </View>
                            <View style={styles.small}>
                                <Image source={require('./../content/images/yellow-card.png')} />
                            </View>
                        </View>
                    }
                    renderItem={({ item }) =>


                        <View
                            key={item.Id}
                            style={{
                                flex: 1,
                                flexDirection: 'row'
                                , padding: 10
                            }}>


                            <View style={[styles.small]}>
                                <Text style={styles.dataStyle}>{item.No}</Text>
                            </View>
                            <View style={styles.big}>
                                <TouchableOpacity onPress={() => { navigate('FootballerDetail', { Id: item.Id }) }}>
                                    <Text style={styles.dataStyle} numberOfLines={2}>{item.Name}</Text>

                                </TouchableOpacity>
                            </View>
                            <View style={[styles.medium,]}>
                                <Text style={styles.dataStyle}>{item.Position}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.Age}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.Minute}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.MainPlayer}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.Goal}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.RedCard}</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={styles.dataStyle}>{item.YellowCard}</Text>
                            </View>
                        </View>} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    big: {
        flex: 5.4,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    small: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    medium: {
        flex: 4,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    textStyle: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600',
    },
    dataStyle:{
        color: '#1c1c1c',
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 20,
    }
})

export default TeamDetailSquadView