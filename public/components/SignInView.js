import React, { Component, PropTypes } from 'react';
import {
    View,
    TextInput,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    Dimensions
} from 'react-native';
import { AuthManager } from './../app/AuthManager'
import Spinner from 'react-native-loading-spinner-overlay';
import KeyboardSpace from 'react-native-keyboard-space';

const { height, width } = Dimensions.get('window');
class SignInView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            errormessage: '',
            loading: false
        }
    }
    loginUser() {
        this.setState({ errormessage: '', loading: true });
        AuthManager.login(this.state.username, this.state.password, (e) => {
            if (e.status == "failed") {
                this.setState({ errormessage: 'Hatalı giriş yaptınız' });
            } else {
                this
                    .props
                    .login(e.username, this.state.password);
                this
                    .props
                    .setToken(e.data);
                this
                    .props
                    .isAuthenticated(this.props.userInfo, true);
                this
                    .props
                    .navigation
                    .navigate("Home")
            }
            this.setState({ loading: false });
        });
    }
    render() {
        return (
            <KeyboardAvoidingView
                behavior='padding'
                style={{
                    backgroundColor: '#ffffff',
                    height: height,
                    width: width
                }}>
                <Spinner visible={this.state.loading} />
                <ScrollView
                    style={{
                        flex: 1,
                        flexDirection: 'column'
                    }}>
                    <View
                        style={{
                            alignItems: 'center',
                            padding: 20
                        }}>
                        <View
                            style={[
                                styles.viewContainer, {
                                    marginBottom: 50,
                                    marginTop: 30
                                }
                            ]}>
                            <Image source={require('./../content/images/golegol-big.png')} />
                        </View>
                        <View style={styles.viewContainer}>
                            <View
                                style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                <Text style={styles.textStyle}>E-POSTA ADRESİ</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={(text) => this.setState({ username: text })}
                                    underlineColorAndroid='rgba(0,0,0,0)' />
                            </View>
                        </View>
                        <View style={styles.viewContainer}>
                            <View
                                style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                <Text style={styles.textStyle}>ŞİFRE</Text>
                                <TextInput
                                    style={styles.textInput}
                                    secureTextEntry={true}
                                    onChangeText={(text) => this.setState({ password: text })}
                                    underlineColorAndroid='rgba(0,0,0,0)' />
                            </View>
                        </View>
                        <View style={styles.viewContainer}>
                            <View
                                style={{
                                    flex: 1
                                }}>
                                <TouchableOpacity style={styles.buttonWrapper} onPress={() => this.loginUser()}>
                                    <Text style={styles.loginButtonStyle}>GİRİŞ YAPIN</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <KeyboardSpace />
                        {this.state.errormessage != '' && (
                            <View style={[styles.errorContainer]}>
                                <View style={styles.error}>
                                    <Text style={styles.loginButtonStyle}>
                                        {this.state.errormessage}
                                    </Text>
                                </View>
                            </View>
                        )}
                        {/* <View style={[styles.forgotPasswordWrapper]}>
                            <View style={{}}>
                                <TouchableOpacity>
                                    <Text style={styles.forgotPassword}>Şifremi unuttum</Text>
                                </TouchableOpacity>
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}
export default SignInView
const styles = StyleSheet.create({
    forgotPasswordWrapper: {
        flexDirection: 'row'
    },
    forgotPassword: {
        color: '#5d5d5d',
        fontSize: 11,
        fontWeight: '400',
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'center',
        textAlign: 'center',
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        textDecorationColor: "#5d5d5d"
    },
    errorContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    error: {
        backgroundColor: '#d64d4d',
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorText: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '400'
    },
    buttonWrapper: {
        height: 35,
        backgroundColor: '#e4b713',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButtonStyle: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '700'
    },
    viewContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    textInput: {
        height: 40,
        borderColor: '#c8c8c8',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        opacity: .8,
    },
    textStyle: {
        color: '#1b1b1b',
        fontSize: 11,
        fontWeight: '600',
        marginBottom: 5
    }
})