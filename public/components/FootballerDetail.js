import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { paths } from './../app/helpers/apipaths'
import moment from 'moment';
import { ConnectionManager } from './../app/ConnectionManager'
import { APP_CONFIG } from './../app/Config'

import { FootballerDetailTabs } from './../routes'

class FootVallerDetailView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            footballerId: props.navigation.state.params.Id,
            footballerDetail: {
                Team: {
                    TeamLogo: {
                        LogoPath: null
                    }},
                PlayerInfo: {
                    ImagePath: null
                }
            }
        }
        console.log(this.state.footballerId)
    }
    componentDidMount() {
        this.fetchReponses();
    }
    fetchReponses() {
        ConnectionManager.request(paths.footballerDetail + "/" + this.state.footballerId, {
            completed: function (h) {
                this.setState({
                    footballerDetail: h
                });
            }.bind(this)
        });
    }
    render() {
        let footballerDetail = this.state.footballerDetail;
        let imageAvaible = footballerDetail.PlayerInfo.ImagePath != null;
        let footballerImage = APP_CONFIG.domain + footballerDetail.PlayerInfo.ImagePath;

        let teamImageAvaible = footballerDetail.Team.TeamLogo.LogoPath != null;
        let teamImage = APP_CONFIG.domain + "/assets/images/" + footballerDetail.Team.TeamLogo.LogoPath;
        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={{
                    padding: 10, flexDirection: 'row',
                    alignItems: 'center'
                }}>
                    <View style={{}}>
                        {imageAvaible && (
                            <Image style={{ marginRight: 10, width: 49, height: 55, borderWidth: 1 }}
                                source={{ uri: footballerImage }}
                            />
                        )}
                    </View>
                    <View >
                        <Text style={{
                            fontSize: 18,
                            fontWeight: '600',
                            color: '#3f3f3f',
                            marginBottom: 5
                        }}>{this.state.footballerDetail.Name}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            {teamImageAvaible && (
                                <Image style={{ marginRight: 3, width: 11, height: 12, borderWidth: 1 }}
                                    source={{ uri: teamImage }}
                                />
                            )}
                            <Text style={{
                                fontSize: 11,
                                fontWeight: '400',
                                color: '#3f3f3f',
                            }}>{this.state.footballerDetail.Team.Name}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex:1}}>
                    <FootballerDetailTabs screenProps={{
                        state: this.state,
                        props: this.props
                    }} />
                </View>
            </View>
        )
    }
}

export default FootVallerDetailView