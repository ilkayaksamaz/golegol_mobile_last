import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';

export default class DetailHeader extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let props = this.props;
        let star = props.starIsEnabled ?  <TouchableOpacity style={styles.buttonStyle}>
                        <Image
                            style={styles.backViewImage}
                            source={require("../../content/images/header-star.png")}></Image>
                    </TouchableOpacity> : null;
        return (
            <View style={styles.container}>
                <View style={styles.backView}>
                    <TouchableOpacity style={styles.buttonStyle} onPress={() => props.back()}>
                        <Image
                            style={styles.backViewImage}
                            source={require("../../content/images/header-arrow.png")}></Image>
                    </TouchableOpacity>
                </View>
                <View style={styles.titleView}>
                    <Text style={styles.title}>{props.title}</Text>
                </View>
                <View style={styles.starView}>
                   {star}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#e4b713',
        justifyContent: 'flex-end',
        height: 44
    },
    backView: {
        flex: 0.2,
        alignItems: 'center',
        flexDirection: 'row'
    },
    backViewImage: {
        alignSelf: 'center'
    },
    titleView: {
        flex: 0.6,
        alignItems: 'center',
        flexDirection: 'row'
    },
    title: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        color: '#ffffff',
        fontSize: 18,
        fontWeight: '600'
    },
    starView: {
        flex: 0.2,
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonStyle: {
        flex: 1
    }
})