import React, {Component} from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import Star from './../HelperComponentes/BookmarkStar'
import {AuthManager} from './../../app/AuthManager'

class FootballLine extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: false
        }
    }
    componentDidMount() {
        this.setState({selected: this.props.item.IsSelected});
    }
    onPress(address, id, type, name) {
        const {navigate} = this.props.navigation;
        if (type == 1) {
            navigate(address, {
                id: id,
                type,
                entityName: address,
                selected: address == "TeamDetail"
                    ? false
                    : this.state.selected,
                name: name
            });
        }
    }
    render() {
        let props = this.props.item;
        return (
            <View style={[styles.view, styles.borderBottom, props.Goal == 1 ? styles.GaolBackround : null]}>
                <View style={[styles.ms]}>
                    <TouchableOpacity
                        style={[styles.buttonStyle]}
                        onPress={() => this.onPress("MatchDetail", props.Id, props.SportType, props.HomeName + " - " + props.AwayName)}>
                        <Text style={[styles.text, styles.textStyle]} numberOfLines={1}>{props.Minute}</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.homeTeam]}>
                    <TouchableOpacity
                        style={[styles.buttonStyle]}
                        onPress={() => this.onPress("TeamDetail", props.HomeId, props.SportType, props.HomeName)}>
                        <Text style={[styles.text, styles.textStyle]} numberOfLines={5}>{props.HomeName}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.score}>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={() => this.onPress("MatchDetail", props.Id, props.SportType, props.HomeName + " - " + props.AwayName)}>
                        <Text style={[styles.text, styles.textStyle, styles.bold,props.Goal == 1 ? styles.GaolText : null]} numberOfLines={5}>
                            {props.ScoreStr}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.awayTeam}>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={() => this.onPress("TeamDetail", props.AwayId, props.SportType, props.AwayName)}>
                        <Text style={[styles.text, styles.textStyle]} numberOfLines={5}>{props.AwayName}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.ms}>
                    <Star
                        updateSelected={(isSelected) => {
                            this.setState({selected: isSelected})
                    
                    }}
                        selected={props.IsSelected}
                        eventId={props.Id}
                        sportType={props.SportType}
                        name={props.HomeName + " - " + props.AwayName}
                        screenProps={this.props}/>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    GaolBackround:{
        backgroundColor:'#f4ff9c'
    },
    GaolText:{
        color:'#f00'
    },
    view: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 8,
        paddingRight: 8
    },
    textStyle: {
        color: '#1c1c1c',
        fontSize: 12
    },
    bold: {
        fontWeight: 'bold'

    },
    borderBottom: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#e7e7e7'
    },
    homeTeam: {
        flex: 0.35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    ms: {
        flex: 0.20,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    score: {
        flex: 0.20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    awayTeam: {
        flex: 0.35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonStyle: {
        flex: 1
    },
    text: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 2,
        paddingRight: 2,
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default FootballLine