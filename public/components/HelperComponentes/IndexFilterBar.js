import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Button
} from 'react-native'
import { ConnectionManager } from './../../app/ConnectionManager'
import { DatePicker } from './../../app/helpers/DatePicker'
import { PickerWrapper } from './../../app/helpers/Picker'
import { paths } from './../../app/helpers/apipaths'
import DateTimePicker from 'react-native-modal-datetime-picker';

const footballIconActive = require("./../../content/images/football-icon.png");
const footballIconPassive = require("./../../content/images/football-icon-passive.png");

const basketballIconActive = require("./../../content/images/basketball-icon.png");
const basketballIconPassive = require("./../../content/images/basketball-icon-passive.png");

class IndexFilterBar extends Component {
    constructor(props) {
        super(props)
        this._openDatePicker = this
            ._openDatePicker
            .bind(this);

        this._handleDatePicked = this
            ._handleDatePicked
            .bind(this);

        this.state = {
            startEnabled: false,
            isDateTimePickerVisible: false
        }
    }
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        console.log(day, month, year);
        this
            .props
            .setCurrentLiveScoreDate({ action: date.action, day: day, month: month, year: year });
        this
            .props
            .uptedateFunction();
    };

    _openDatePicker(props) {
        this._showDateTimePicker();
    }
    render() {
        const { navigate } = this.props.navigation;

        let props = this.props;

        const activeIcon = require('./../../content/images/star-active.png');
        const passiveIcon = require('./../../content/images/star-passive.png');
        let footbalIcon = this.props.parts == "Football"
            ? footballIconActive
            : footballIconPassive;
        let footballBackground = this.props.parts == "Football"
            ? styles.IconActive
            : styles.IconPassive;

        let basketBallIcon = this.props.parts == "Basketball"
            ? basketballIconActive
            : basketballIconPassive;

        let basketBallBackground = this.props.parts == "Basketball"
            ? styles.IconActive
            : styles.IconPassive;
        let league = this
            .props
            .leagues
            .find(function (m) {
                return m.value == props.selectedLeague;
            });
        let currentLeague = league == undefined
            ? "Tüm Ligler"
            : league.label;
        return (

            <View>
                <View style={styles.container}>
                    <TouchableOpacity
                        style={[styles.sportIcon, footballBackground, styles.IconMargin]}
                        onPress={() => {
                            this
                                .props
                                .setCurrentPart("Football");
                            this
                                .props
                                .uptedateFunction();
                        }}>
                        <Image source={footbalIcon}></Image>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.sportIcon, basketBallBackground, styles.IconMargin]}
                        onPress={() => {
                            this
                                .props
                                .setCurrentPart("Basketball");
                            this
                                .props
                                .uptedateFunction();
                        }}>
                        <Image source={basketBallIcon}></Image>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.IconMargin]}
                        onPress={() => {
                            this
                                .props
                                .setLive(this.props.isLive == true
                                    ? false
                                    : true);
                            this
                                .props
                                .uptedateFunction();
                        }}>
                        <View
                            style={[
                                this.props.isLive
                                    ? styles.TextWrapper
                                    : styles.PassiveTextWrapper, {
                                    width: 40,
                                    height: 25
                                }
                            ]}>
                            <Text style={[styles.TextIcon]}>Canlı</Text>
                        </View>
                    </TouchableOpacity>
                    <View
                        style={[
                            styles.IconMargin,
                            styles.TextWrapper, {
                                width: 90,
                                height: 25
                            }
                        ]}>

                        <TouchableOpacity
                            style={[styles.IconMargin]}
                            onPress={() => {
                                navigate('LeaguesList');
                                this
                                    .props
                                    .uptedateFunction();
                            }}>
                            <View
                                style={[
                                    styles.TextWrapper, {
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        height: 25
                                    }
                                ]}>
                                <Text
                                    numberOfLines={1}
                                    style={[
                                        styles.TextIcon, {
                                            overflow: 'hidden'
                                        }
                                    ]}>{currentLeague}</Text>
                            </View >
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.IconMargin]}>
                        <TouchableOpacity
                            onPress={() => {
                                this._openDatePicker(this.props);
                            }}>
                            <Image
                                source={require("../../content/images/datepicker-icon.png")}
                                style={{
                                    width: 15,
                                    height: 15
                                }}></Image>
                        </TouchableOpacity>
                        <DateTimePicker
                            date={new Date(this.props.dates.year, this.props.dates.month, this.props.dates.day)}
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker} />

                    </View>

                    <View style={[styles.IconMargin]}>
                        <TouchableOpacity
                            onPress={() => {
                                var res = !this.state.startEnabled;
                                this.setState({
                                    startEnabled: res
                                }, () => {
                                    this
                                        .props
                                        .starEnabled(res);
                                    this
                                        .props
                                        .uptedateFunction();
                                })
                            }}>

                            {this.state.startEnabled
                                ? <Image
                                    source={require("../../content/images/star-active.png")}
                                    style={{
                                        width: 15,
                                        height: 15
                                    }}></Image>
                                : <Image
                                    source={require("../../content/images/star-passive.png")}
                                    style={{
                                        width: 15,
                                        height: 15
                                    }}></Image>}
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={[
                        styles.IconActive, {
                            flexDirection: 'row'
                        }
                    ]}>
                    <View
                        style={[
                            styles.filterColumns, {
                                marginLeft: 20
                            },
                            (this.props.order == "byLeague"
                                ? null
                                : styles.filterColumnPassive)
                        ]}>
                        <TouchableOpacity
                            onPress={() => {
                                this
                                    .props
                                    .setCurrentOrderType("byLeague");
                                this
                                    .props
                                    .uptedateFunction();
                            }}
                            style={styles.filterColumnButton}>
                            <Text style={styles.filterColumnText}>Lige Göre Sıralı</Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={[
                            styles.filterColumns, {
                                marginRight: 20,
                                marginLeft: 10
                            },
                            (this.props.order == "byDate"
                                ? null
                                : styles.filterColumnPassive)
                        ]}>
                        <TouchableOpacity
                            onPress={() => {
                                this
                                    .props
                                    .setCurrentOrderType("byDate");
                                this
                                    .props
                                    .uptedateFunction();
                            }}
                            style={styles.filterColumnButton}>
                            <Text style={styles.filterColumnText}>Tarihe Göre Sıralı</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    filterColumns: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    filterColumnText: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: "bold"
    },
    filterColumnPassive: {
        opacity: 0.3
    },
    filterColumnButton: {
        backgroundColor: '#3f3f3f',
        paddingLeft: 10,
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'column',
        alignSelf: 'stretch',
        marginTop: 10
    },
    container: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        flexDirection: 'row',
        flex: 0,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    TextWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e4b713'
    },
    PassiveTextWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3f3f3f'
    },
    TextIcon: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600',
        textAlign: 'center'
    },
    sportIcon: {
        width: 45,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    IconMargin: {
        marginRight: 8
    },
    IconActive: {
        backgroundColor: '#0e8d38ff'
    },
    IconPassive: {
        backgroundColor: '#c3c3c3'
    }
})

export default IndexFilterBar
