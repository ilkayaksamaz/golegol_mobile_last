import React, { Component } from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { AuthManager } from './../../app/AuthManager'
class Star extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        };
    }

    componentDidMount() {
        this.setState({ selected: this.props.selected })
    }
    SetBookmark(entityId, type, isSelected) {
        let { token, address, userInfo, navigation, notificationIsDisabled } = this.props.screenProps;
        let entityName = address == undefined
            ? "MatchDetail"
            : address;
        if (userInfo.isAuthenticated) {
            if (!notificationIsDisabled) {
                AuthManager
                    .addBookmark(token, entityId, entityName, this.props.name, type, isSelected)
                    .then((res) => {
                        this.setState({ selected: res })
                        this
                            .props
                            .updateSelected(res);
                    })
            } else {
                navigation.navigate("Settings");
            }
        } else {
            navigation.navigate("SignIn");
        }
    }
    render() {
        const activeIcon = require('./../../content/images/star-active.png');
        const passiveIcon = require('./../../content/images/star-passive.png');
        let starPath = this.state.selected
            ? activeIcon
            : passiveIcon;
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={() => {
                        this.SetBookmark(this.props.eventId, this.props.sportType, this.state.selected)
                    }}>
                    <Image source={starPath} style={styles.star}></Image>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    star: {
        width: 16,
        height: 16
    }
});
export default Star