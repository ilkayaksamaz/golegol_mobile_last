import React, { Component } from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image,
    ScrollView
} from 'react-native'

import { paths } from './../../app/helpers/apipaths'
import moment from 'moment';
import { ConnectionManager } from './../../app/ConnectionManager'
import { APP_CONFIG } from './../../app/Config'
export default class MatchDetailView extends Component {
    constructor(props) {
        super(props);
    }

    _fetchResponses() {
        let props = this.props.screenProps;
        ConnectionManager.request(paths.matchInfo + "/" + props.navigation.state.params.id, {
            completed: function (e) {
                props.setSelectedMatchInfo(e);
            }.bind(this)
        });

    }

    _fetchSquadReponses() {
        let props = this.props.screenProps;
        if (props.matchdetail.Home.Id != undefined) {
            ConnectionManager.request(paths.squad + "/" + props.matchdetail.Home.Id, {
                completed: function (h) {
                    props.setSelectedMatchHomeSquad(h);
                }
            });
            ConnectionManager.request(paths.squad + "/" + props.matchdetail.Away.Id, {
                completed: function (a) {
                    props.setSelectedMatchAwaySquad(a);
                }
            });
        }
    }
    componentDidMount() {
        this._fetchResponses();
        this._fetchSquadReponses();
    }

    shouldComponentUpdate(nextProps, nextState) {
        let props = this.props.screenProps;
        if (props.matchdetailinfo == undefined) {
            return false
        }
        if (props.matchdetail != undefined && props.matchdetail.Home != undefined && props.matchdetail.Home.Id != undefined) {

            return true
        }
        return true;
    }
    _getIconName(eventType, event) {
        let iconName = eventType;
        if (eventType == "CARD") {
            iconName = (event.CardType == 1
                ? "RED"
                : "YELLOW") + eventType;
        }
        return iconName;

    }
    _getIcon(type) {
        const goalIcon = require('../../content/images/goal.png');
        const redcard = require('../../content/images/red-card.png');
        const yellowcard = require('../../content/images/yellow-card.png');
        const playerIn = require('../../content/images/in.png');
        const playerOut = require('../../content/images/out.png');

        switch (type) {
            case "GOAL":
                return goalIcon;
                break;
            case "PLAYERCHANGEOUT":
                return playerOut;
                break;
            case "PLAYERCHANGEIN":
                return playerIn;
                break;
            case "REDCARD":
                return redcard;
                break;
            case "YELLOWCARD":
                return yellowcard;
                break;
        }
    }

    _keyExtractor = (item, index) => item.Id;
    navigateToFootballer(id) {
        let { navigate } = this.props.screenProps.navigation;
        navigate("FootballerDetail", { Id: id })
    }
    render() {

        let props = this.props.screenProps;
        let content = props
            .matchdetailinfo
            .map((p, i) => <View key={i} style={styles.container}>
                <View
                    style={[
                        styles.containerPart, {
                            alignItems: 'flex-end'
                        }
                    ]}>

                    {p.Side == 1 && (
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>

                            {p.Event.EventType == "PLAYERCHANGE"
                                ? (
                                    <View>
                                        <View
                                            style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'flex-end',
                                                alignItems: 'center'
                                            }}>
                                            <Image source={this._getIcon("PLAYERCHANGEOUT")} />
                                            <Text
                                                style={{
                                                    fontSize: 10
                                                }}>{p.Event.OutPlayer.Name}</Text>
                                        </View>
                                        <View
                                            style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}>
                                            <Image source={this._getIcon("PLAYERCHANGEIN")} />
                                            <Text style={styles.footballerName}>{p.Event.Player.Name}</Text>
                                        </View>
                                    </View>
                                )
                                : (
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <View
                                            style={{
                                                marginRight: 5
                                            }}>
                                            {p.Event.EventType == "GOAL" && p.Event.Asist != undefined && p.Event.Asist != null && (
                                                <Text numberOfLines={5} style={styles.footballerName}>{p.Event.Asist.Name}</Text>
                                            )}
                                            <Text numberOfLines={5} style={styles.footballerName}>{p.Event.Player.Name}</Text>
                                        </View>
                                        <View>
                                            <Image source={this._getIcon(this._getIconName(p.Event.EventType, p.Event))} />
                                        </View>
                                    </View>
                                )}
                        </View>
                    )}
                </View>
                <View style={styles.containerPartMinute}>
                    <View style={styles.minuteWrapper}>
                        <Text style={styles.minute}>{p.MatchTime.Time}'</Text>
                    </View>
                </View>
                <View
                    style={[
                        styles.containerPart, {
                            alignItems: 'flex-start',

                        }
                    ]}>
                    {p.Side == 2 && (
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>

                            {p.Event.EventType == "PLAYERCHANGE"
                                ? (
                                    <View>
                                        <View
                                            style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center'
                                            }}>
                                            <Image source={this._getIcon("PLAYERCHANGEOUT")} />
                                            <Text
                                                numberOfLines={5}
                                                style={{
                                                    fontSize: 10
                                                }}>{p.Event.OutPlayer.Name}</Text>
                                        </View>
                                        <View
                                            style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}>
                                            <Image source={this._getIcon("PLAYERCHANGEIN")} />
                                            <Text numberOfLines={5} style={styles.footballerName}>{p.Event.Player.Name}</Text>
                                        </View>
                                    </View>
                                )
                                : (
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <View>
                                            <Image source={this._getIcon(this._getIconName(p.Event.EventType, p.Event))} />
                                        </View>
                                        <View
                                            style={{
                                                marginLeft: 5
                                            }}>
                                            {p.Event.EventType == "GOAL" && p.Event.Asist != undefined && p.Event.Asist != null && (
                                                <Text numberOfLines={5} style={styles.footballerName}>{p.Event.Asist.Name}</Text>
                                            )}
                                            <Text style={styles.footballerName}>{p.Event.Player.Name}</Text>
                                        </View>

                                    </View>
                                )}
                        </View>
                    )}

                </View>

            </View>)

        let homeSquad = props.homesquad;
        return (
            <ScrollView
                style={{
                    flex: 1,
                    paddingTop: 0,
                    backgroundColor: '#ffffff'
                }}>
                <View
                    style={{
                        paddingLeft: 20,
                        paddingRight: 20
                    }}>
                    {content}
                </View>
                <View>
                    <View
                        style={{
                            backgroundColor: '#3f3f3f',
                            paddingLeft: 20,
                            paddingRight: 20,
                            paddingBottom: 5,
                            paddingTop: 5,
                        }}>
                        <Text
                            style={{
                                color: '#ffffff',
                                fontSize: 11,
                                fontWeight: '600'
                            }}>
                            Kadro
                        </Text>
                    </View>
                    <View>
                        <View>
                            <Text
                                style={{
                                    backgroundColor: '#5d5d5d',
                                    paddingLeft: 20,
                                    paddingRight: 20,
                                    paddingBottom: 5,
                                    paddingTop: 5,
                                    color: '#ffffff',
                                    fontSize: 11,
                                    fontWeight: '600'
                                }}>
                                {props.matchdetail.Home.Name + " Kadrosu"}
                            </Text>
                            <FlatList
                                data={props.homesquad}
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item }) => <View
                                    key={item.Id}
                                    style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        borderBottomWidth: 1,
                                        borderBottomColor: '#e7e7e7',
                                        paddingBottom: 3,
                                        paddingTop: 3
                                    }}>
                                    <View
                                        style={{
                                            marginRight: 10
                                        }}>
                                        <Text
                                            style={{
                                                fontSize: 14,
                                                fontWeight: '400',
                                                lineHeight: 20,
                                                color: '#1c1c1c'
                                            }}>{item.No}</Text>
                                    </View>
                                    <View style={{}}>
                                        <TouchableOpacity onPress={() => this.navigateToFootballer(item.Id)}>
                                            <Text
                                                style={{
                                                    fontSize: 14,
                                                    fontWeight: '400',
                                                    lineHeight: 20,
                                                    color: '#1c1c1c'
                                                }}>{item.Name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>} />
                        </View>
                        <View>
                            <Text
                                style={{
                                    backgroundColor: '#5d5d5d',
                                    paddingLeft: 20,
                                    paddingRight: 20,
                                    paddingBottom: 5,
                                    paddingTop: 5,
                                    color: '#ffffff',
                                    fontSize: 11,
                                    fontWeight: '600'
                                }}>
                                {props.matchdetail.Away.Name + " Kadrosu"}
                            </Text>
                            <FlatList
                                data={props.awaysquad}
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item }) => <View
                                    key={item.Id}
                                    style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        paddingLeft: 20,
                                        paddingRight: 20,
                                        borderBottomWidth: 1,
                                        borderBottomColor: '#e7e7e7',
                                        paddingBottom: 3,
                                        paddingTop: 3
                                    }}>
                                    <View
                                        style={{
                                            marginRight: 10
                                        }}>
                                        <Text
                                            style={{
                                                fontSize: 14,
                                                fontWeight: '400',
                                                lineHeight: 20,
                                                color: '#1c1c1c'
                                            }}>{item.No}</Text>
                                    </View>
                                    <View style={{}}>
                                        <TouchableOpacity onPress={() => this.navigateToFootballer(item.Id)}>
                                            <Text
                                                style={{
                                                    fontSize: 14,
                                                    fontWeight: '400',
                                                    lineHeight: 20,
                                                    color: '#1c1c1c'
                                                }}>{item.Name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>} />
                        </View>
                    </View>
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 5,
        borderBottomColor: '#e7e7e7',
        borderBottomWidth: 1,
        marginBottom: 10
    },
    containerPart: {
        flex: .4,
        justifyContent: 'center'
    },
    containerPartMinute: {
        flex: .2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    footballerName: {
        color: '#1c1c1c',
        fontSize: 14,
        fontWeight: '400'
    },
    minuteWrapper: {
        borderColor: '#757575',
        borderStyle: 'solid',
        borderWidth: 4,
        backgroundColor: '#757575',
        borderRadius: 4,
        marginTop: 5,
        alignItems: 'center',
        paddingLeft: 3,
        paddingRight: 3
    },
    minute: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'center'
    }
})