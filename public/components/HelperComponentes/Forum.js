import React, {Component} from 'react'
import {
    FlatList,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native'

import {paths} from './../../app/helpers/apipaths'
import moment from 'moment';
import {ConnectionManager} from './../../app/ConnectionManager'
import {APP_CONFIG} from './../../app/Config'
import {AuthManager} from './../../app/AuthManager'
import KeyboardSpace from 'react-native-keyboard-space';

class ForumList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comment: "",
            allComments: [],
            page: props.page != undefined
                ? props.page
                : 1,
            refreshing: false
        };

    }
    componentWillUnmount() {}
    componentDidMount() {
        this._fetchResponses();
    }

    _addComment(commentText) {}
    _fetchResponses() {
        let props = this.props;
        let uri = paths.forum + "/" + props.entityId + "/" + props.entityName + "?page=" + (this.state.page);
        ConnectionManager.request(uri, {
            completed: function (e) {
                this.setState({
                    refreshing: false,
                    allComments: this
                        .state
                        .allComments
                        .concat(e)
                })
            }.bind(this)
        });
    }

    _onRefresh = () => {
        this.setState({
            refreshing: true,
            page: 1
        }, () => {
            this.setState({allComments: []});
            this._fetchResponses();
        });
    }
    handleLoadMore = () => {
        let page = this.state.page;
        this.setState({
            refreshing: true,
            page: page + 1
        }, () => {
            this._fetchResponses();
        });
    }
    _keyExtractor = (item, index) => item.Id;
    render() {
        let {userInfo, token} = this.props.screenProps;
        let isEditable = userInfo.isAuthenticated;
        return (
            <View
                style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: '#ffffff'
            }}>
                <View
                    style={{
                    flex: .9,
                    flexDirection: 'row',
                    alignSelf: 'flex-start'
                }}>
                    <FlatList
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        onRefresh={this._onRefresh}
                        refreshing={this.state.refreshing}
                        data={this.state.allComments}
                        keyExtractor={this._keyExtractor}
                        style={{
                        flex: 1
                    }}
                        renderItem={({item, index}) => <View
                        style={{
                        flex: 1,
                        backgroundColor: index % 2 == 0
                            ? '#ffffff'
                            : '#e7e7e7'
                    }}>
                        <View
                            style={{
                            flex: 1,
                            flexDirection: 'row',
                            paddingLeft: 10,
                            paddingRight: 10,
                            paddingTop: 5,
                            paddingBottom: 5
                        }}>
                            <Text
                                style={{
                                color: '#1c1c1c',
                                fontSize: 12,
                                fontWeight: '600',
                                marginRight: 10
                            }}>{item.UserName}</Text>
                            <Text
                                style={{
                                color: '#1c1c1c',
                                fontSize: 12,
                                fontWeight: '600'
                            }}>{item.ForJsDate}</Text>
                        </View>
                        <View
                            style={{
                            flex: 1,
                            flexDirection: 'row',
                            paddingLeft: 10,
                            paddingRight: 10,
                            paddingTop: 5,
                            paddingBottom: 5
                        }}>
                            <Text
                                style={{
                                color: '#6f6f6f',
                                fontSize: 14,
                                fontWeight: '400'
                            }}>{item.Message}</Text>
                        </View>
                    </View>}/>
                </View>
                <View
                    style={{
                    flexDirection: 'row',
                    alignSelf: 'flex-end',
                    height: 70
                }}>
                    {isEditable
                        ? (

                            <View
                                style={{
                                flex: 1,
                                backgroundColor: '#aabfc8',
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 15,
                                paddingBottom: 15,
                                flexDirection: 'row'
                            }}>
                                <View
                                    style={{
                                    flex: .9
                                }}>
                                    <TextInput
                                        onChangeText={(text) => this.setState({comment: text})}
                                        value={this.comment}
                                        editable={isEditable}
                                        selectTextOnFocus={isEditable}
                                        style={{
                                        backgroundColor: '#ffffff',
                                        flex: 1,
                                        flexDirection: 'row'
                                    }}
                                        underlineColorAndroid='transparent'></TextInput>
                                </View>
                                <View
                                    style={{
                                    flex: .1,
                                    backgroundColor: '#ffffff',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                        if (this.state.comment.length > 0) {
                                            AuthManager
                                                .addComment(token, this.state.comment, this.props.entityId, "Football")
                                                .then(() => {
                                                    this._onRefresh();
                                                    this.setState({comment: ""})
                                                })
                                        }
                                    }}
                                        style={{
                                        alignSelf: 'center'
                                    }}>
                                        <Image source={require('./../../content/images/comment-button.png')}/>
                                    </TouchableOpacity>
                                </View>
                                <KeyboardSpace />
                            </View>
                        )
                        : (
                            <View
                                style={{
                                backgroundColor: '#aabfc8',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <View
                                    style={{
                                    flex: 1,
                                    justifyContent: 'center'
                                }}>
                                    <Text
                                        style={{
                                        color: '#135775',
                                        fontSize: 11,
                                        fontWeight: '700'
                                    }}>Yorum yapmak için</Text>
                                </View>
                                <View
                                    style={{
                                    flex: 1,
                                    flexDirection: 'row'
                                }}>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                            this
                                                .props
                                                .screenProps
                                                .navigation
                                                .navigate("SignIn")
                                        }}
                                            style={{
                                            alignSelf: 'center'
                                        }}>
                                            <Image source={require('./../../content/images/login-button.png')}/>
                                        </TouchableOpacity>
                                    </View>
                                    <View
                                        style={{
                                        marginRight: 10,
                                        marginLeft: 10
                                    }}>
                                        <Text
                                            style={{
                                            color: '#135775',
                                            fontSize: 11,
                                            fontWeight: '400'
                                        }}>Veya</Text>
                                    </View>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                            this
                                                .props
                                                .screenProps
                                                .navigation
                                                .navigate("SignUp")
                                        }}
                                            style={{
                                            alignSelf: 'center'
                                        }}>
                                            <Image source={require('./../../content/images/signup-button.png')}/>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )
}
                </View>
            </View>
        )
    }
}

export default ForumList