import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image,
    ScrollView
} from 'react-native'

import {paths} from './../../app/helpers/apipaths'
import moment from 'moment';
import {ConnectionManager} from './../../app/ConnectionManager'
import {APP_CONFIG} from './../../app/Config'

export default class MatchCompare extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Compratison: {
                HomeWin: 0,
                AwayWin: 0,
                Draw: 0,
                Total: 0
            },
            HomeComparison: {
                HomeWin: 0,
                AwayWin: 0,
                Draw: 0,
                Total: 0
            },
            AllMatchesBetween: [],
            HomeMatches: [],
            AwayMatches: []
        }
    }

    _fetchResponses() {
        let props = this.props.screenProps;
        let current = this;
        ConnectionManager.request(paths.matchcompare + "/" + props.matchdetail.Id, {
            completed: function (e) {
                this.setState({Compratison: e.Compratison, HomeComparison: e.HomeComparison, AllMatchesBetween: e.AllMatchesBetween, HomeMatches: e.HomeMatches, AwayMatches: e.AwayMatches})
            }.bind(this)
        });
    }
    componentDidMount() {
        this._fetchResponses();
    }

    shouldComponentUpdate(nextProps, nextState) {
        // if (this.props.matchdetail.Home === undefined) {     return false }
        return true;
    }
    render() {
        let props = this.props.screenProps;
        return (
            <ScrollView
                style={{
                flex: 1,
                flexDirection: 'column'
            }}>
                <View style={styles.section}>
                    <View style={[styles.listHeader]}>
                        <Text style={[styles.listHeaderText]}>Aralarındaki Son 10 Maç</Text>
                    </View>
                    <View style={[styles.contentWrapper]}>
                        <FlatList
                            data={this.state.AllMatchesBetween}
                            keyExtractor={(item, index) => item.HomeId + "_" + item.AwayId}
                            renderItem={({item}) => <View style={[styles.contentContainer]}>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .2
                                }
                            ]}>{moment(item.EventDate).format("DD.MM.YY")}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.HomeName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.HomeScore}
                                - {item.AwayScore}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.AwayName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.fikstur_ilkyari}</Text>
                        </View>}/>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={[styles.listHeader]}>
                        <Text style={[styles.listHeaderText]}>{props.matchdetail.Home.Name  + " "}
                            Form Durumu</Text>
                    </View>
                    <View style={[styles.contentWrapper]}>
                        <FlatList
                            data={this.state.HomeMatches}
                            keyExtractor={(item, index) => item.HomeId + "_" + item.AwayId}
                            renderItem={({item}) => <View style={[styles.contentContainer]}>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .2
                                }
                            ]}>{moment(item.EventDate).format("DD.MM.YY")}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.HomeName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.HomeScore}
                                - {item.AwayScore}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.AwayName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.fikstur_ilkyari}</Text>
                        </View>}/>
                    </View>
                </View>

                <View style={styles.section}>
                    <View style={[styles.listHeader]}>
                        <Text style={[styles.listHeaderText]}>{props.matchdetail.Away.Name + " "}
                            Form Durumu</Text>
                    </View>
                    <View style={[styles.contentWrapper]}>
                        <FlatList
                            data={this.state.AwayMatches}
                            keyExtractor={(item, index) => item.HomeId + "_" + item.AwayId}
                            renderItem={({item}) => <View style={[styles.contentContainer]}>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .2
                                }
                            ]}>{moment(item.EventDate).format("DD.MM.YY")}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.HomeName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.HomeScore}
                                - {item.AwayScore}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .3
                                }
                            ]}>{item.AwayName}</Text>
                            <Text
                                style={[
                                styles.content, {
                                    flex: .1
                                }
                            ]}>{item.fikstur_ilkyari}</Text>
                        </View>}/>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={[styles.listHeader]}>
                        <Text style={[styles.listHeaderText]}>Aralarınaki Maçlar</Text>
                    </View>
                    <View style={[styles.contentWrapper]}>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Galibiyet</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.HomeWin}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.Total}</Text>
                            </View>

                        </View>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Beraberlik</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.Draw}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.Total}</Text>
                            </View>

                        </View>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Mağlubiyet</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.AwayWin}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.Compratison.Total}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={[styles.listHeader]}>
                        <Text style={[styles.listHeaderText]}>{props.matchdetail.Home.Name + " "}
                            Evineki Maçlar</Text>
                    </View>
                    <View style={[styles.contentWrapper]}>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Galibiyet</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.HomeWin}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.Total}</Text>
                            </View>

                        </View>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Beraberlik</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.Draw}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.Total}</Text>
                            </View>

                        </View>
                        <View style={styles.infoBars}>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>Mağlubiyet</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.AwayWin}</Text>
                            </View>
                            <View style={styles.infoBarContainer}>
                                <Text style={[styles.contentText]}>{this.state.HomeComparison.Total}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    infoBars: {
        flexDirection: 'row',
        paddingRight: 10,
        paddingLeft: 10
    },
    infoBarContainer: {
        flex: .2
    },
    contentText: {
        color: '#1c1c1c',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19.85
    },
    section: {
        flex: 1,
        marginBottom: 10
    },
    listHeader: {
        height: 25,
        backgroundColor: '#3f3f3f',
        justifyContent: 'center',
        paddingRight: 10,
        paddingLeft: 10
    },
    listHeaderText: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600'
    },
    contentWrapper: {
        flexDirection: 'column'
    },
    contentContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingRight: 10,
        paddingLeft: 10
    },
    content: {
        color: '#1c1c1c',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 19.85,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    }
})