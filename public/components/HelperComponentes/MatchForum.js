import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image
} from 'react-native'

import Forum from './Forum';

export default class MatchForum extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let props = this.props.screenProps;
        return (
            <View style={{
                flex: 1,

            }}>
                <Forum entityId={props.navigation.state.params.id} entityName='Football' screenProps={props}/>
            </View>
        );
    }
}