import React from 'react'
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { AuthManager } from './../../app/AuthManager'
const Header = (props) => (
    <View style={styles.backround}>
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                flex: 0.6,
            }}>
            <Image
                style={[styles.headerBoxes]}
                source={require('../../content/images/header-back.png')}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center'
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            props
                                .navigation
                                .navigate("DrawerOpen");
                        }}>
                        <Image
                            source={require('../../content/images/left-menu-icon.png')}
                            style={styles.menuIcon}></Image>
                    </TouchableOpacity>
                    <Image source={require('../../content/images/logo.png')} style={styles.logo}></Image>
                </View>
            </Image>
        </View>
        <View
            style={[{
                flex: .4,
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
                paddingRight: 10
            }
            ]}>
            <View style={styles.row}>
                {!props.userInfo.isAuthenticated ? (<View style={[styles.icons]}>
                    <TouchableOpacity
                        onPress={() => {
                            props
                                .navigation
                                .navigate("SignIn");
                        }}>
                        <Image source={require('../../content/images/user-icon.png')}></Image>
                    </TouchableOpacity>
                </View>) : (null)}

                <View style={[styles.icons]}>

                    <TouchableOpacity
                        onPress={() => {
                            props
                                .navigation
                                .navigate("Search");
                        }}>
                        <Image source={require('../../content/images/search-icon.png')}></Image>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </View >
)

const styles = StyleSheet.create({
    logo: {
        marginLeft: 15,
        width: 115,
        height: 17
    },
    menuIcon: {
        width: 18,
        height: 20
    },
    headerBoxes: {
        justifyContent: 'flex-start'
    },
    icon: {
        marginRight: 10
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    icons: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        width: 60
    },
    backround: {
        backgroundColor: '#e4b713ff',
        flex: 0,
        flexDirection: 'row'
    },
    haederBackImage: {}
})

export default Header
