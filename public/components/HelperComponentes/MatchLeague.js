import React, { Component } from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image
} from 'react-native'

import { paths } from './../../app/helpers/apipaths'
import moment from 'moment';
import { ConnectionManager } from './../../app/ConnectionManager'
import { APP_CONFIG } from './../../app/Config'

export default class MatchLeague extends Component {
    constructor(props) {
        super(props);
    }

    _fetchResponses() {
        let props = this.props.screenProps;
        if (props.matchdetail.League.Id != undefined) {
            ConnectionManager.request(paths.league + "/" + props.matchdetail.League.Id, {
                completed: function (e) {
                    props.setSelectedMatchLeague(e);
                }
            });
        }
    }
    componentDidMount() {
        this._fetchResponses();
    }

    shouldComponentUpdate(nextProps, nextState) {
        let props = this.props.screenProps;
        if (props.matchdetail.League.Id === undefined) {
            return false
        }
        return true;
    }

    _keyExtractor = (item, index) => item.takim_id;
    navigateToTeam(id) {
        let { navigate } = this.props.screenProps.navigation;
        navigate("TeamDetail", { id: id })
    }
    render() {

        let props = this.props.screenProps;
        return (
            <View style={{
                backgroundColor: '#ffffff'
            }}>
                <View
                    style={{
                        backgroundColor: '#3f3f3f',
                        paddingTop: 8,
                        paddingBottom: 8,
                        paddingLeft: 20,
                        paddingRight: 20
                    }}>
                    <Text
                        style={{
                            color: '#ffffff',
                            fontSize: 11,
                            fontWeight: '600'
                        }}>{props.matchdetail.League.Name}</Text>
                </View>
                <View style={{}}>
                    <FlatList
                        data={props.matchdetailleague}
                        keyExtractor={this._keyExtractor}
                        ListHeaderComponent={() => <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                backgroundColor: '#5d5d5d',
                                paddingTop: 8,
                                paddingBottom: 8
                            }}>
                            <View style={styles.big}>
                                <Text style={[styles.text, styles.color]}>Takım</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>O</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>G</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>B</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>M</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>A</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>Y</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>P</Text>
                            </View>
                            <View style={styles.small}>
                                <Text style={[styles.text, styles.color]}>AV</Text>
                            </View>
                        </View>}
                        renderItem={({ item }) => <View
                            style={{
                                flex: 1,
                                flexDirection: 'row'
                            }}>
                            <View style={[styles.big, styles.margin]}>
                                <TouchableOpacity onPress={() => {
                                    this.navigateToTeam(item.takim_id)
                                }}>
                                    <Text style={styles.text}>{item.takim_ad}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_o}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_g}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_b}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_m}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_a}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_y}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={styles.text}>{item.oz_puan}</Text>
                            </View>
                            <View style={[styles.small, styles.margin]}>
                                <Text style={[styles.text]}>{item.oz_av}</Text>
                            </View>
                        </View>} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    big: {
        flex: 0.5,
        paddingLeft: 20,
        paddingRight: 20
    },
    small: {
        flex: 0.08
    },
    margin: {
        paddingBottom: 4,
        paddingTop: 4,
        borderBottomWidth: 1,
        borderBottomColor: '#e7e7e7'
    },
    text: {
        color: '#1c1c1c',
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 20
    },
    color: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600'
    }
})