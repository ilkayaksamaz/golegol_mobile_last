import React, {Component} from 'react'
import {
    FlatList,
    Picker,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image
} from 'react-native'
import {paths} from './../app/helpers/apipaths'
import {LeagueScoreDetailTabs} from './../routes'

import {ConnectionManager} from './../app/ConnectionManager'

export default class LeagueScoreView extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {}
    render() {
        let props = this.props;
        const {navigate} = props.navigation;
       
        let league = this
            .props
            .leagues
            .find(function (m) {
                return m.value == props.selectedLeague;
            });
        let currentLeague = league == undefined
            ? "Tüm Ligler"
            : league.label;

        return (
            <View
                style={{
                flexDirection: 'column',
                flex: 1
            }}>
                <View
                    style={{
                    flexDirection: 'row',
                    height: 50,
                    backgroundColor: '#5d5d5d',
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingTop: 10,
                    paddingBottom: 10
                }}>
                    <View style={{
                        flex: 1
                    }}>
                        <View
                            style={{
                            flex: 1,
                            backgroundColor: '#ffffff',
                            flexDirection: 'row',
                            padding: 5,
                            marginRight: 10
                        }}>
                            <TouchableOpacity
                                style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                                onPress={() => {
                                navigate('LeaguesList');
                            }}>

                                <View
                                    style={{
                                    flex: .9
                                }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                        color: '#3f3f3f',
                                        fontSize: 11,
                                        fontWeight: '600'
                                    }}>{currentLeague}</Text>
                                </View>
                                <View
                                    style={{
                                    flex: .1,
                                    flexDirection: 'row'
                                }}>
                                    <Image source={require('./../content/images/dropdown-arrow.png')}/>

                                </View>

                            </TouchableOpacity>
                        </View>

                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <View
                            style={{
                            flex: 1,
                            backgroundColor: '#ffffff',
                            flexDirection: 'row',
                            padding: 5
                        }}>

                            <View
                                style={{
                                flex: 1,
                                flexDirection: 'row'
                            }}>

                                <TouchableOpacity
                                    style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                                    onPress={() => {
                                    navigate('SeasonPicker');
                                }}>

                                    <View
                                        style={{
                                        flex: .9
                                    }}>
                                        <Text
                                            numberOfLines={1}
                                            style={{
                                            color: '#3f3f3f',
                                            fontSize: 11,
                                            fontWeight: '600'
                                        }}>{props.season}</Text>
                                    </View>
                                    <View
                                        style={{
                                        flex: .1,
                                        flexDirection: 'row'
                                    }}>
                                        <Image source={require('./../content/images/dropdown-arrow.png')}/>
                                    </View>

                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </View>
                <View
                    style={{
                    flexDirection: 'row',
                    flex: 1
                }}>
                    <LeagueScoreDetailTabs screenProps={props} onChange={() => {}}/>
                </View>
            </View>
        );
    }
}