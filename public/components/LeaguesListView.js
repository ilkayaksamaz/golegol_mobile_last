import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet
} from 'react-native'
import {paths} from './../app/helpers/apipaths'

import {ConnectionManager} from './../app/ConnectionManager'

export default class LeaguesListView extends Component {
    constructor(props) {
        super(props);

        this.onSearchChange = this
            .onSearchChange
            .bind(this);
        this.state = {
            searchTerm: ""
        };
    }
    onSearchChange(text) {
        this.setState({searchTerm: text});
    }
   
    componentDidMount() {
        let localProps = this.props;
        ConnectionManager.request(paths.leagueList, {
            completed: function (e) {
                localProps.setAllLeagues(e);
            }
        }, true, "allLeagues");
    }
    render() {
        let props = this.props;
        let state = this.state;
        let {goBack} = this.props.navigation;
        let filteredAssets = state.searchTerm.length <= 0
            ? this.props.leagues
            : this
                .props
                .leagues
                .filter(function (i) {
                    return i
                        .label
                        .toLowerCase()
                        .search(state.searchTerm.toLowerCase()) != -1;
                });

        return (
            <View style={styles.container}>
                <View style={styles.textContainer}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.input}
                        onChangeText={(text) => this.onSearchChange(text)}
                        value={this.state.searchTerm}/>
                </View>
                <View style={styles.textContainer}>
                    <FlatList
                        automaticallyAdjustContentInsets={false}
                        data={filteredAssets}
                        keyExtractor={(item, index) => item.value}
                        renderItem={({item}) => <TouchableOpacity
                        onPress={() => {
                        props.setSelectedLeague(item.value);
                        goBack();
                    }}>
                        <Text>
                            {item.label}
                        </Text>
                    </TouchableOpacity>}/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 20,
        backgroundColor: '#ffffff'
    },
    textContainer: {
        flexDirection: 'row',
        flex: 0,
        alignItems: 'stretch'
    },
    input: {
        height: 35,
        alignItems: 'stretch',
        flex: 1,
        marginBottom: 20,

        color: '#828282',
        fontSize: 14,
        fontWeight: '400',
        borderColor: '#c8c8c8',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        opacity: 0.8
    }
})