import React, {Component} from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet
} from 'react-native'
import {paths} from './../app/helpers/apipaths'

import {ConnectionManager} from './../app/ConnectionManager'

export default class LeaguesListView extends Component {
    constructor(props) {
        super(props);
        this.onSearchChange = this
            .onSearchChange
            .bind(this);
        this.state = {
            searchTerm: "Ara...",
            Items: []
        };
    }
    onSearchChange(text) {
        this.setState({
            searchTerm: text
        }, () => {
            if (this.state.searchTerm.length >= 3) {
                setTimeout(() => {
                    ConnectionManager.request(paths.search + "/" + this.state.searchTerm, {
                        completed: function (e) {
                            this.setState({Items: e})
                        }.bind(this)
                    });
                }, 500)
            }
        });
    }
    render() {
        let props = this.props;
        let state = this.state;
        let {goBack, navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.Input}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.inputItem}
                        onChangeText={(text) => this.onSearchChange(text)}
                        onFocus={() => {
                        if (this.state.searchTerm === "Ara...") {
                            this.setState({searchTerm: ""});
                        }
                    }}
                        value={this.state.searchTerm}/>
                </View>
                <View style={styles.textContainer}>
                    <FlatList
                        automaticallyAdjustContentInsets={false}
                        data={this.state.Items}
                        ItemSeparatorComponent={() => < View style = {{ height: 1, backgroundColor: '#ffffff', opacity:.2, marginBottom:3,marginTop:3 }}></View>}
                        keyExtractor={(item, index) => item.ID}
                        ListHeaderComponent={() => <View style={[styles.headerStyle]}>
                        <Text style={[styles.headerTextStyle]}>Popüler aramalar</Text>
                    </View>}
                        renderItem={({item}) => <TouchableOpacity
                        onPress={() => {
                        switch (item.DataType) {
                            case 1:
                                navigate("FootballerDetail", {Id: item.ID});
                                break;
                            case 2:
                                navigate("TeamDetail", {id: item.ID});
                                break;
                            case 3:
                                props.setSelectedLeague(item.ID);
                                navigate("LeagueScore", {Id: item.ID});
                                break;
                        }
                    }}>
                        <View
                            style={{
                            paddingLeft: 10,
                            paddingRight: 10,
                            paddingTop: 3,
                            paddingBottom: 3
                        }}>
                            <Text
                                style={{
                                color: '#ffffff',
                                fontSize: 14,
                                fontWeight: '600'
                            }}>
                                {item.Name}
                            </Text>
                        </View>
                    </TouchableOpacity>}/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    headerStyle: {
        height: 25,
        backgroundColor: '#3f3f3f',
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center'
    },
    headerTextStyle: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '600'
    },
    Input: {
        backgroundColor: '#fff',
        borderColor: '#000'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
    },
    textContainer: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#e4b713'
    },
    inputItem: {
        width: 249,
        height: 44,
        color: '#1b1b1b',
        fontSize: 18,
        fontWeight: '600',
        paddingLeft: 10,
        paddingRight: 10
    }
})