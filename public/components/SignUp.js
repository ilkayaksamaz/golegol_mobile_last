import React, {Component, PropTypes} from 'react';
import {
    View,
    TextInput,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Image,
    Dimensions,
    KeyboardAvoidingView
} from 'react-native';
import {AuthManager} from './../app/AuthManager'
import Spinner from 'react-native-loading-spinner-overlay';
import KeyboardSpace from 'react-native-keyboard-space';

class SignInView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            User: {
                Password: "",
                RePassword: "",
                UserName: "",
                Email: "",
                Phone: "",
                loading: false
            },
            ErrorList: []
        }
    }

    loginUser() {
        this.setState({loading: true, ErrorList: []});
        AuthManager.login(this.state.User.Email, this.state.User.Password, (e) => {
            if (e.status == "failed") {
                this.setState({errormessage: 'Hatalı giriş yaptınız'});
            } else {
                this
                    .props
                    .login(e.username, this.state.Password);
                this
                    .props
                    .setToken(e.data);
                this
                    .props
                    .isAuthenticated(this.props.userInfo, true);
                this
                    .props
                    .navigation
                    .navigate("Home")
            }
            this.setState({loading: false});
        });
    }
    render() {
        let errors = this
            .state
            .ErrorList
            .map((i) => <View style={styles.error}>
                <Text style={styles.loginButtonStyle}>{i}</Text>
            </View>)
        return (
            <View
                style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: '#ffffff'
            }}
                automaticallyAdjustContentInsets={false}>
                <Spinner visible={this.state.loading}/>
                <View
                    style={{
                    flex: .8,
                    padding: 20,
                    flexDirection: 'row'
                }}>
                    <View
                        style={{
                        flex: 1,
                        alignItems: 'center',
                        padding: 20,
                        flexDirection: 'row'
                    }}>
                        <ScrollView
                            behavior="position"
                            style={{
                            flex: 1
                        }}>
                            <View
                                style={[
                                styles.viewContainer, {
                                    marginBottom: 50,
                                    marginTop: 0,
                                    justifyContent: 'center'
                                }
                            ]}>
                                <Image source={require('./../content/images/golegol-big.png')}/>
                            </View>

                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                    <Text style={styles.textStyle}>E-POSTA ADRESİ</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        onChangeText={(text) => this.state.User.Email = text}
                                        underlineColorAndroid='rgba(0,0,0,0)'/>
                                </View>
                            </View>
                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                    <Text style={styles.textStyle}>KULLANICI ADI</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        onChangeText={(text) => this.state.User.UserName = text}
                                        underlineColorAndroid='rgba(0,0,0,0)'/>

                                </View>
                            </View>
                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                    <Text style={styles.textStyle}>TELEFON</Text>
                                    <TextInput
                                        onChangeText={(text) => this.state.User.Phone = text}
                                        style={styles.textInput}
                                        underlineColorAndroid='rgba(0,0,0,0)'/>
                                </View>
                            </View>
                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                    <Text style={styles.textStyle}>ŞİFRE</Text>
                                    <TextInput
                                        secureTextEntry={true}
                                        onChangeText={(text) => this.state.User.Password = text}
                                        style={styles.textInput}
                                        underlineColorAndroid='rgba(0,0,0,0)'/>
                                </View>
                            </View>

                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1,
                                    height: 55
                                }}>
                                    <Text style={styles.textStyle}>ŞİFRE TEKRAR</Text>
                                    <TextInput
                                        secureTextEntry={true}
                                        onChangeText={(text) => this.state.User.RePassword = text}
                                        style={styles.textInput}
                                        underlineColorAndroid='rgba(0,0,0,0)'/>
                                </View>
                            </View>
                            <View style={styles.viewContainer}>
                                <View
                                    style={{
                                    flex: 1
                                }}>
                                    <TouchableOpacity
                                        style={styles.buttonWrapper}
                                        onPress={() => {
                                        this.setState({loading: true, ErrorList: []});
                                        AuthManager
                                            .create(this.state.User)
                                            .then((result) => {
                                                if (result.Status == 1) {
                                                    this.loginUser();
                                                } else {
                                                    this.setState({ErrorList: result.ErrorsList});
                                                    this.setState({loading: false});
                                                }
                                            })
                                            .catch(() => this.setState({loading: false}))
                                    }}>
                                        <Text style={styles.loginButtonStyle}>ÜYE OLUN</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardSpace />
                            
                            <View style={styles.errorContainer}>
                                {errors}
                            </View>
                        </ScrollView>

                    </View>
                </View>
            </View>
        )
    }
}
export default SignInView
const styles = StyleSheet.create({
    forgotPasswordWrapper: {
        flexDirection: 'row'
    },
    forgotPassword: {
        height: 12,
        color: '#5d5d5d',
        fontSize: 11,
        fontWeight: '400',
        marginTop: 30,
        marginBottom: 30,
        justifyContent: 'center',
        textAlign: 'center',
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        textDecorationColor: "#5d5d5d"
    },
    errorContainer: {
        flexDirection: 'row',
        marginTop: 10
    },
    error: {
        backgroundColor: '#d64d4d',
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    errorText: {
        color: '#ffffff',
        fontSize: 11,
        fontWeight: '400'
    },
    buttonWrapper: {
        height: 35,
        backgroundColor: '#e4b713',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButtonStyle: {
        color: '#ffffff',
        fontSize: 14,
        fontWeight: '700'
    },
    viewContainer: {
        flexDirection: 'row',
        marginTop: 20
    },
    textInput: {
        height: 40,
        borderColor: '#c8c8c8',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        opacity: .8
    },
    textStyle: {
        color: '#1b1b1b',
        fontSize: 11,
        fontWeight: '600',
        marginBottom: 5
    }
})