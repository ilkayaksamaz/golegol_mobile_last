import React, { Component } from 'react'
import { Platform, Switch, Text, View } from 'react-native'
import FCM from 'react-native-fcm';
import Spinner from 'react-native-loading-spinner-overlay';
import { AuthManager } from './../app/AuthManager'

class SettingView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false
        }
    }
    DisableSound(isDisabled) {
        this.props.DisableSounds(isDisabled);
    }
    DisableNotification(isDisabled) {
        this.props.DisableNotifications(isDisabled);
        if (isDisabled) {
            this.setState({ loading: true })
            AuthManager.getUserInfo().then(info => {
                if (info.token && info.token != undefined) {
                    AuthManager.getbookmark(info.token).then((res) => {
                        for (var i in res) {
                            var bookmark = res[i];
                            var fcmRes = FCM.unsubscribeFromTopic(bookmark.EntityName + "_" + bookmark.EntityId);
                            console.log(fcmRes);
                            AuthManager.addBookmark(info.token, bookmark.EntityId, bookmark.EntityName, "", 1, true);
                        }
                    });
                }
            })
                .then(() => this.setState({ loading: false }))
                .catch(() => {
                    this.setState({ loading: false });
                });
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    padding: 20,
                    backgroundColor: '#ffffff'
                }}>
                <Spinner visible={this.state.loading} />

                <View style={{}}>
                    {/* <View
                        style={{
                            flexDirection: 'row'
                        }}>
                        <View style={{
                            flex: 1
                        }}>
                            <Text
                                style={{
                                    color: '#1c1c1c',
                                    fontSize: 14,
                                    fontWeight: '400',
                                    lineHeight: 20
                                }}>Sesler</Text>
                        </View>
                        <View style={{
                            flex: 1
                        }}>
                            <Switch
                                style={{
                                    marginBottom: 10
                                }}
                                onTintColor="#4bd562"
                                thumbTintColor="#ffffff"
                                tintColor="#4bd562"
                                onValueChange={(value) => this.DisableSound(value)}
                                style={{
                                    marginBottom: 10
                                }}
                                value={this.props.soundIsDisabled} />
                        </View>
                    </View> */}
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row'
                        }}>
                        <View style={{
                            flex: 1
                        }}>
                            <Text
                                style={{
                                    color: '#1c1c1c',
                                    fontSize: 14,
                                    fontWeight: '400',
                                    lineHeight: 20
                                }}>Anlık bildirimler</Text>
                        </View>
                        <View style={{
                            flex: 1
                        }}>
                            <Switch
                                style={{
                                    marginBottom: 10
                                }}
                                onValueChange={(value) => this.DisableNotification(!value)}
                                style={{
                                    marginBottom: 10
                                }}
                                value={!this.props.notificationIsDisabled} />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default SettingView