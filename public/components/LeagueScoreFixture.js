import React, { Component } from 'react'
import {
    FlatList,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StyleSheet,
    Image
} from 'react-native'
import { paths } from './../app/helpers/apipaths'
import moment from 'moment'
import { ConnectionManager } from './../app/ConnectionManager'

export default class LeagueScoreStaticsFixture extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fixtureList: []
        }

        this.fetchResponses(this.props.selectedLeague, this.props.season);
    }
    componentDidMount() { }
    fetchResponses(league, season) {
        let uri = paths.leagueFixture + "?leagueId=" + league + "&seasonId=" + season;
        ConnectionManager.request(uri, {
            completed: function (e) {
                this.setState({ fixtureList: e })
            }.bind(this)
        });

    }

    _keyExtractor = (item, index) => "" + item.HomeId + item.AwayId;
    componentWillReceiveProps(nextProps) {
        this.setState({ selectedLeague: nextProps.selectedLeague, season: nextProps.season })
        if (this.props.selectedLeague !== nextProps.selectedLeague) {
            this.fetchResponses(nextProps.selectedLeague, nextProps.season);
        }
        if (this.props.season !== nextProps.season) {
            this.fetchResponses(nextProps.selectedLeague, nextProps.season);
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    nagigateToTeam(teamId) {
        const { navigate } = this.props.screenProps.navigation;
        navigate("TeamDetail", { id: teamId })
    }


    nagigateToMatch(id) {
        const { navigate } = this.props.screenProps.navigation;
        navigate("MatchDetail", { id: id })
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    marginTop: 5,
                    marginBottom: 5,

                    flex: 1,
                    backgroundColor: "#e7e7e7",
                }}
            />
        );
    };
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                <View style={{
                    backgroundColor: '#0e8d38',
                    paddingBottom: 10,
                    paddingTop: 10,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{
                        color: '#ffffff',
                        fontSize: 14,
                        fontWeight: '400',
                    }}>HAFTANIN MAÇLARI</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', padding: 20 }}>
                    <FlatList
                        ItemSeparatorComponent={this.renderSeparator}

                        data={this.state.fixtureList}
                        keyExtractor={this._keyExtractor}
                        style={{}}
                        renderItem={({ item, index }) => <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: 10,
                            }}>
                            <View style={{ flex: .35, justifyContent: 'flex-start'}}>
                                <Text style={{
                                    color: '#1c1c1c',
                                    fontSize: 14,
                                    textAlign:'left',
                                }}>{moment(item.EventDate).format("DD.MM HH:SS")}</Text>
                            </View>
                            <View style={{ flex: .4, alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={() => this.nagigateToTeam(item.HomeId)}>
                                    <Text style={{
                                        color: '#1c1c1c',
                                        fontSize: 14,
                                        textAlign:'right',
                                    }}>{item.HomeName}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: .2, alignItems: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.nagigateToMatch(item.Id)}>
                                    <Text style={{
                                        color: '#1c1c1c',
                                        fontSize: 14,
                                        textAlign:'center',
                                    }}>{item.HomeScore} - {item.AwayScore}</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ flex: .4, alignItems: 'flex-start' }}>
                                <TouchableOpacity
                                    onPress={() => this.nagigateToTeam(item.AwayId)}>
                                    <Text style={{
                                        color: '#1c1c1c',
                                        fontSize: 14,
                                        textAlign:'left',
                                    }}>{item.AwayName}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>} />
                </View>
            </View>
        );

    }
}
