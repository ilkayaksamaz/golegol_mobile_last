import {connect} from 'react-redux'
import {spinnerClose, spinnerOpen} from '../actions/leaguescorestatics'

import LeagueScoreStaticsView from '../components/LeagueScoreStaticsView'

const mapStateToProps = (state) => ({leagues: state.leagues, season: state.season, nav: state.nav, selectedLeague: state.selectedLeague});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(LeagueScoreStaticsView)
