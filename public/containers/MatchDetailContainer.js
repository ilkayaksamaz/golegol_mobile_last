import {connect} from 'react-redux'
import {
    spinnerClose,
    spinnerOpen,
    setSelectedMatch,
    setSelectedMatchInfo,
    setSelectedMatchHomeSquad,
    setSelectedMatchAwaySquad,
    setSelectedMatchLeague
} from '../actions/matchdetailactions'

import MatchDetailView from '../components/MatchDetailView'

const mapStateToProps = (state) => ({
    matchdetail: state.matchdetail,
    matchdetailleague: state.matchdetailleague,
    matchdetailinfo: state.matchdetailinfo,
    homesquad: state.homesquad,
    awaysquad: state.awaysquad,
    nav: state.nav,
    userInfo: state.userInfo,
    token: state.token,
    soundIsDisabled: state.soundIsDisabled,
    notificationIsDisabled: state.notificationIsDisabled,
});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    setSelectedMatch: (matchdetail) => {
        dispatch(setSelectedMatch(matchdetail))
    },
    setSelectedMatchInfo: (matchdetail) => {
        dispatch(setSelectedMatchInfo(matchdetail))
    },
    setSelectedMatchHomeSquad: (matchdetail) => {
        dispatch(setSelectedMatchHomeSquad(matchdetail))
    },
    setSelectedMatchAwaySquad: (matchdetail) => {
        dispatch(setSelectedMatchAwaySquad(matchdetail))
    },
    setSelectedMatchLeague: (league) => {
        dispatch(setSelectedMatchLeague(league))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(MatchDetailView)
