import {connect} from 'react-redux'
import {setAllLeagues, setSelectedLeague, spinnerClose, spinnerOpen} from '../actions/leaguescore'

import LeaguesScoreView from '../components/LeagueScore'

const mapStateToProps = (state) => ({leagues: state.leagues, season: state.season, nav: state.nav, selectedLeague: state.selectedLeague});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    setSelectedLeague: (league) => {
        dispatch(setSelectedLeague(league))
    },
    setAllLeagues: (league) => {
        dispatch(setAllLeagues(league))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LeaguesScoreView)
