import { connect } from 'react-redux'
import { disableNotifications, disableSounds } from '../actions/settings'

import SettingsView from '../components/Settings'

const mapStateToProps = (state) => ({
    nav: state.nav,
    soundIsDisabled: state.soundIsDisabled,
    notificationIsDisabled: state.notificationIsDisabled,
});

const mapDispatchToProps = (dispatch) => ({
    DisableSounds: (isDisabled) => {
        dispatch(disableSounds(isDisabled))
    },
    DisableNotifications: (isDisabled) => {
        dispatch(disableNotifications(isDisabled))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsView)
