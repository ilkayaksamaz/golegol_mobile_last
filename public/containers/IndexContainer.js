import {connect} from 'react-redux'
import {
    getMatches,
    setCurrentPart,
    getCurrentPart,
    setCurrentLiveScoreDate,
    getCurrentLiveScoreDate,
    setCurrentOrderType,
    setSelectedLeague,
    clearMatches,
    setAllLeagues,
    setLive,
    spinnerClose,
    spinnerOpen,
    starEnabled,
    matchDate
} from '../actions/index'
import IndexComponent from '../components/index'

const mapStateToProps = (state) => ({
    matches: state.matches,
    parts: state.parts,
    dates: state.dates,
    order: state.order,
    selectedLeague: state.selectedLeague,
    leagues: state.leagues,
    isLive: state.isLive,
    nav: state.nav,
    spinner: state.spinner,
    userInfo: state.userInfo,
    token: state.token,
    isStartEnabled: state.isStartEnabled,
    soundIsDisabled: state.soundIsDisabled,
    notificationIsDisabled: state.notificationIsDisabled,
});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    starEnabled: (isStartEnabled) => {
        dispatch(starEnabled(isStartEnabled))
    },
    onGetMatches: (match) => {
        dispatch(getMatches(match))
    },
    setCurrentPart: (part) => {
        dispatch(setCurrentPart(part))
    },
    getCurrentPart: () => {
        return dispatch(getCurrentPart())
    },
    setCurrentLiveScoreDate: (date) => {
        dispatch(setCurrentLiveScoreDate(date))
    },
    getCurrentLiveScoreDate: () => {
        return dispatch(getCurrentLiveScoreDate())
    },
    setCurrentOrderType: (order) => {
        dispatch(setCurrentOrderType(order))
    },
    setSelectedLeague: (league) => {
        dispatch(setSelectedLeague(league))
    },
    setAllLeagues: (league) => {
        dispatch(setAllLeagues(league))
    },
    clearMatches: () => {
        dispatch(clearMatches())
    },
    setLive: (isLive) => {
        dispatch(setLive(isLive))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexComponent)