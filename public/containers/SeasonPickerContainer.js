import {connect} from 'react-redux'
import {setSelectedSeason,spinnerClose, spinnerOpen} from '../actions/seasonpicker'

import SeasonPickerView from '../components/SeasonPickerView'

const mapStateToProps = (state) => ({season: state.season, nav: state.nav});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    setSelectedSeason: (season) => {
        dispatch(setSelectedSeason(season))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SeasonPickerView)
