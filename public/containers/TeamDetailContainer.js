import {connect} from 'react-redux'
import {spinnerClose, spinnerOpen} from '../actions/teamdetail'

import TeamDetailView from '../components/TeamDetailView'

const mapStateToProps = (state) => ({nav: state.nav});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamDetailView)
