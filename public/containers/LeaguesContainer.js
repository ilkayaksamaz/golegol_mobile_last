import {connect} from 'react-redux'
import {setSelectedLeague, setAllLeagues, spinnerClose, spinnerOpen} from '../actions/leagues'

import LeaguesListView from '../components/LeaguesListView'

const mapStateToProps = (state) => ({leagues: state.leagues, nav: state.nav});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    setSelectedLeague: (league) => {
        dispatch(setSelectedLeague(league))
    },
    setAllLeagues: (league) => {
        dispatch(setAllLeagues(league))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LeaguesListView)
