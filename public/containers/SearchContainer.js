import {connect} from 'react-redux'
import {setSelectedLeague} from '../actions/leagues'

import Search from '../components/Search'

const mapStateToProps = (state) => ({leagues: state.leagues, nav: state.nav});

const mapDispatchToProps = (dispatch) => ({
    openSpinner: () => {
        dispatch(spinnerOpen())
    },
    closeSpinner: () => {
        dispatch(spinnerClose())
    },
    setSelectedLeague: (league) => {
        dispatch(setSelectedLeague(league))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Search)
